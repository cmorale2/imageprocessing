# Image Processing
## Connor Morales

***
This repo consist of work I have done for classes and continuations of that work I've pursued as a hobby.  The algorithms are mainly taken from [Digital Image Processing, Third Edition by Gonzales and Woods](http://www.imageprocessingplace.com/index.htm).  The images are those provided with the textbook.

***

