%% Histogram Package Demo
% Author: Connor Morales


%% Cleanup
close all
clear
clc
pause(0.1)


%% Histogram Equalization/Linearization

img = cell(2,1);
img{1} = imread('../Images/pollen_dark.tif');
img{2} = imread('../Images/pollen_light.tif');

hist = cell(2,1);
hist{1} = histogram.compute(img{1});
hist{2} = histogram.compute(img{2});

imgNew = cell(2,1);
fprintf('Linearize...');
t = tic();
imgNew{1} = histogram.linearize(img{1});
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Linearize...');
t = tic();
imgNew{2} = histogram.linearize(img{2});
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

histNew = cell(2,1);
histNew{1} = histogram.compute(imgNew{1});
histNew{2} = histogram.compute(imgNew{2});

figure('Name','Histogram Equalization - Dark', 'ToolBar','none');
subplot(2,2,1)
imshow(img{1});
title('Original');
subplot(2,2,2)
imshow(imgNew{1})
title('Equalized');
subplot(2,2,3)
bar(0:255, hist{1}, 'b');
xlim([0,255]);
subplot(2,2,4)
bar(0:255, histNew{1}, 'b');
xlim([0,255]);

figure('Name','Histogram Equalization - Light', 'ToolBar','none');
subplot(2,2,1)
imshow(img{2});
title('Original');
subplot(2,2,2)
imshow(imgNew{2})
title('Equalized');
subplot(2,2,3)
bar(0:255, hist{2}, 'b');
xlim([0,255]);
subplot(2,2,4)
bar(0:255, histNew{2}, 'b');
xlim([0,255]);

pause(0.1);


