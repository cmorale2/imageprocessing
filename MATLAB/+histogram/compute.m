%% Histogram Analysis - Histogram Computation
% Author: Connor Morales
% 
% Description:
%   This function takes an intensity image and outputs a histogram of its
%   values.
%   
% Input:
%   (1) - Input intensity image
%
% Output:
%   (1) - Histogram values (0:255)
% 

function [hist] = compute(imgIn)

	hist = zeros(256,1);
	
	for i = 1 : length(hist)
		hist(i) = sum(imgIn(:) == (i-1));
	end
	
end
