%% Histogram Analysis - Normalized Histogram Cumulative Sum
% Author: Connor Morales
% 
% Description:
%   This function takes a normalized histogram and calculates a cumulative
%   sum of it.
%   
% Input:
%   (1) - Normalized histogram (PDF)
%
% Output:
%   (1) - Cumulative histogram sum (CDF)
% 

function [cdf] = cumsum(pdf)

	cdf = zeros(size(pdf));
	cdf(1) = pdf(1);
	for i = 2 : length(pdf)
		cdf(i) = cdf(i-1) + pdf(i);
	end

end
