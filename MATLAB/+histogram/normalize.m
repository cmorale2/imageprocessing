%% Histogram Analysis - Histogram Normalization
% Author: Connor Morales
% 
% Description:
%   This function takes a histogram and normalizes it.
%   
% Input:
%   (1) - Histogram
%
% Output:
%   (1) - Normalized histogram (PDF)
% 

function [pdf] = normalize(hist)

	MN = sum(hist);
	
	pdf = hist / MN;

end
