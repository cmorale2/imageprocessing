%% Histogram Analysis - Linearize Histogram
% Author: Connor Morales
% 
% Description:
%   This function matches the input intensity image's histogram to the
%   provided one as best as possible.
%   
% Input:
%   (1) - Input intensity image
%   (2) - Desired normalized histogram (PDF) (256-length vector)
%
% Output:
%   (1) - Histogram matched intensity image
% 

function [imgOut] = match(imgIn, pdf)

	Gz = 255 * histogram.cumsum(pdf);
	
	imgIn = histogram.linearize(imgIn);
	
	imgOut = zeros(size(imgIn));
	for i = 1 : length(pdf)
		imgOut(imgIn == (i-1)) = findfirstclosest(Gz, (i-1));
	end
	
	imgOut = uint8(round(imgOut));

end





function [ind] = findfirstclosest(cdf, val)

	% Go through all values looking for first match
	for i = 1 : length(cdf)
		% If we matched or went too far, this is the index we want
		if cdf(i) >= val
			break;
		end
	end
	
	ind = i - 1;

end
