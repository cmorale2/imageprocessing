%% Histogram Analysis - Linearize Histogram
% Author: Connor Morales
% 
% Description:
%   This function takes an input intensity image and performs histogram
%   linearization to equalize it.
%   
% Input:
%   (1) - Input intensity image
%
% Output:
%   (2) - Equalized intensity image
% 

function [imgOut] = linearize(imgIn)

	pdf = histogram.normalize(histogram.compute(imgIn));

	s = 255 * histogram.cumsum(pdf);
	
	imgOut = zeros(size(imgIn));
	for i = 1 : length(s)
		imgOut(imgIn == (i-1)) = s(i);
	end
	
	imgOut = uint8(round(imgOut));

end
