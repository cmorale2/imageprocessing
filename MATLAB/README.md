# MATLAB Algorithm Prototyping
## Connor Morales

***
This directory holds all MATLAB code (packages and demo scripts) used for algorithm testing and prototyping.  These packages/functions are not optimized, as they are used to understand and debug algorithms.  For example, the brute force watershed is a direct implementation of the algorithm as described in Digital Image Processing, and it takes several minutes to run on a small, basic image.

***

