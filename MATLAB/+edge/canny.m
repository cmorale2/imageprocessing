%% Edge Detection - Canny
% Author: Connor Morales
% 
% Description:
%   This function performs Canny edge detection on the input image.  The
%   image is smoothed with a Gaussian function with standard deviation as
%   specified.  For hysteresis thresholding, the threshold values can be
%   one of three types:
%     'scale'      - The inputs are multipliers for the max value (hi,low)
%     'percentile' - The first input is the percentile of all non-zero
%                    values to use for the high value.  The low value is
%                    the high value divided by the second input.
%     'values'     - The inputs are raw threshold values (hi,low)
% 
% Input:
%   (1) - Input intensity image
%   (2) - Standard deviation for Gaussian
%   (3) - Type of thresholding
%   (4) - Threshold values (2-length vector)
%
% Output:
%   (1) - Output edge image (logical)
% 

function [imgOut] = canny(imgIn, sigma, ttype, vals)

	% Sizings
	[h,w] = size(imgIn);
	
	% Smooth image
	imgSmooth = spatial.correlate(imgIn, spatial.kernelGaussian(sigma));
	
	% Calculate magnitude and angle
	[M,a] = internal.magang(imgSmooth, 'sqrt');
	
	
	% Perform nonmaxima suppression
	%  - Based on edge direction (horiz,vert,diag), decide on which 2
	%    pixels in N8 should be checked.  If center pixel magnitude is less
	%    than at least one of these two, suppress it (set to 0)
	% NOTE: 0 degrees starts at bottom of unit circle because the edge
	%       location is 90 deg offset from the gradient
	gn = zeros(h,w);
	for x = 1 : w
		for y = 1 : h
			
			% Value to assign
			val = M(y,x);
			
			% Vertical
			if (a(y,x) <= -67.5 && a(y,x) >= -112.5) || (a(y,x) >= 67.5 && a(y,x) <= 112.5)
				% Check top pixel
				if y > 1 && M(y,x) < M(y-1,x)
					val = 0;
				end
				% Check botom pixel
				if y < h && M(y,x) < M(y+1,x)
					val = 0;
				end
				
			% Horizontal
			elseif (a(y,x) >= -22.5 && a(y,x) <= 22.5) || (a(y,x) <= -157.5 || a(y,x) >= 157.5)
				% Check left pixel
				if x > 1 && M(y,x) < M(y,x-1)
					val = 0;
				end
				% Check right pixel
				if x < w && M(y,x) < M(y,x+1)
					val = 0;
				end
			
			% Diagonal tr-bl
			elseif (a(y,x) <= -112.5 && a(y,x) >= -157.5) || (a(y,x) >= 22.5 && a(y,x) <= 67.5)
				% Check TR pixel
				if x < w && y > 1 && M(y,x) < M(y-1,x+1)
					val = 0;
				end
				% Check BL pixel
				if x > 1 && y < h && M(y,x) < M(y+1,x-1)
					val = 0;
				end
				
			% Diagonal tl-br
			else
				% Check TL pixel
				if x > 1 && y > 1 && M(y,x) < M(y-1,x-1)
					val = 0;
				end
				% Check BR pixel
				if x < w && y < h && M(y,x) < M(y+1,x+1)
					val = 0;
				end
				
			end
			
			% Assign value
			gn(y,x) = val;
			
		end
	end
	
	
	% Determine thresholding bounds
	if strcmpi(ttype, 'scale')
		Th = vals(1) * max(gn(:));
		Tl = vals(2) * max(gn(:));
		
	elseif strcmpi(ttype, 'percentile')
		Th = prctile(gn(gn>0), vals(1));
		Tl = Th / vals(2);
		
	elseif strcmpi(ttype, 'values')
		Th = vals(1);
		Tl = vals(2);
		
	end
	
	
	
	% Perform hysteresis thresholding
	gstrong = (gn >= Th);
	gweak   = (gn >= Tl);
	
	% Trim nonzero strong pixels from weak img
	gweak = gweak & ~gstrong;
	
	
	% Create list of all edge pixels
	slist = struct.StackGeneric();
	for x = 1 : w
		for y = 1 : h
			if gstrong(y,x)
				slist.Push([y,x]);
			end
		end
	end
	
	% For all edge pixels, mark all surrounding N8 weak pixels as valid
	% edge pixels.
	tempWeak = false(h,w);
	while ~slist.isEmpty();
		px = slist.Pop();
		x = px(2);
		y = px(1);
		
		% Right
		if x < w && gweak(y,x+1) && ~tempWeak(y,x+1)
			tempWeak(y,x+1) = true;
			slist.Push([y,x+1]);
		% Top Right
		elseif x < w && y > 1 && gweak(y-1,x+1) && ~tempWeak(y-1,x+1)
			tempWeak(y-1,x+1) = true;
			slist.Push([y-1,x+1]);
		% Top
		elseif y > 1 && gweak(y-1,x) && ~tempWeak(y-1,x)
			tempWeak(y-1,x) = true;
			slist.Push([y-1,x]);
		% Top Left
		elseif x > 1 && y > 1 && gweak(y-1,x-1) && ~tempWeak(y-1,x-1)
			tempWeak(y-1,x-1) = true;
			slist.Push([y-1,x-1]);
		% Left
		elseif x > 1 && gweak(y,x-1) && ~tempWeak(y,x-1)
			tempWeak(y,x-1) = true;
			slist.Push([y,x-1]);
		% Bottom Left
		elseif x > 1 && y < h && gweak(y+1,x-1) && ~tempWeak(y+1,x-1)
			tempWeak(y+1,x-1) = true;
			slist.Push([y+1,x-1]);
		% Bottom
		elseif y < h && gweak(y+1,x) && ~tempWeak(y+1,x)
			tempWeak(y+1,x) = true;
			slist.Push([y+1,x]);
		% Bottom Right
		elseif x < w && y < h && gweak(y+1,x+1) && ~tempWeak(y+1,x+1)
			tempWeak(y+1,x+1) = true;
			slist.Push([y+1,x+1]);
		end
		
	end
	
	% Output is strong pixels combined with added weak pixels
	imgOut = gstrong | tempWeak;

end



















