%% Edge Detection - Marr-Hildreth
% Author: Connor Morales
% 
% Description:
%   This function performs Marr-Hildreth edge detection on the input image.
%   The standard deviation and threshold are provided by the user.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Standard deviation
%   (3) - Threshold multiplier (of max value)
%
% Output:
%   (1) - Output edge image (logical)
% 

function [imgOut] = marrhildreth(imgIn, sigma, pcent)

	% Filter with Gaussian
	imgSmooth = spatial.correlate(imgIn, spatial.kernelGaussian(sigma));
	
	% Calculate Laplacian of smoothed
	imgFilt = spatial.correlate(imgSmooth, spatial.kernelLaplace(true));
	
	% Get threshold value
	thresh = pcent * max(imgFilt(:));
	
	% Find zero crossings
	[h,w] = size(imgIn);
	imgOut = false(h,w);
	for x = 2 : w-1
		for y = 2 : h-1
			
			cnt = 0;
			
			% Left-Right
			v1 = imgFilt(y,x-1);
			v2 = imgFilt(y,x+1);
			if sign_compare(v1,v2) && abs(v2+v1) > thresh
				cnt = cnt + 1;
			end
			
			% Top-Bottom
			v1 = imgFilt(y-1,x);
			v2 = imgFilt(y+1,x);
			if sign_compare(v1,v2) && abs(v2+v1) > thresh
				cnt = cnt + 1;
			end
			
			% TL-BR
			v1 = imgFilt(y-1,x-1);
			v2 = imgFilt(y+1,x+1);
			if sign_compare(v1,v2) && abs(v2+v1) > thresh
				cnt = cnt + 1;
			end
			
			% TR-BL
			v1 = imgFilt(y-1,x+1);
			v2 = imgFilt(y+1,x-1);
			if sign_compare(v1,v2) && abs(v2+v1) > thresh
				cnt = cnt + 1;
			end
			
			% Assign output
			imgOut(y,x) = (cnt >= 2);
			
		end
	end

	% Type forcing
	imgOut = logical(imgOut);

end



function tf = sign_compare(v1,v2)

	tf = false;
	
	if sign(v1) ~= sign(v2)
		tf = true;
	end
	
end

