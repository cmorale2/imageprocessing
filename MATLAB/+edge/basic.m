%% Edge Detection - Basic
% Author: Connor Morales
% 
% Description:
%   This function performs basic edge detection on the input image.  The
%   image is first smoothed, then the magnitude is computed.  This image is
%   then thresholded to obtain the edge image.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Optional - Threshold value (multipled by max, default: 0.33)
%   (3) - Optional - Type of smoothing (arithmetic*, geometric, gaussian)
%   (4) - Optional - Smoothing data (size/sigma, default 5)
%
% Output:
%   (1) - Output edge image (logical)
% 

function [imgOut] = basic(imgIn, varargin)

	% Smooth input image
	if nargin > 2
		% Check if we have specified size/sigma
		if nargin > 3
			num = varargin{3};
		else
			num = 5;
		end
		% Perform smoothing
		if strcmpi(varargin{2}, 'arithmetic')
			imgSmooth = spatial.meanArithmetic(imgIn, num);
		elseif strcmpi(varargin{2}, 'geometric')
			imgSmooth = spatial.meanGeometric(imgIn, num);
		elseif strcmpi(varargin{2}, 'gaussian')
			imgSmooth = spatial.correlate(imgIn, spatial.kernelGaussian(num));
		else
			error('Unknown smoothing type: %s', varargin{2});
		end
		
	else
		% Default is arithmetic size 5
		imgSmooth = spatial.meanArithmetic(imgIn, 5);
	end
	
	% Calculate magnitude
	M = internal.magang(imgSmooth, 'sum');
	
	% Threshold
	if nargin > 1
		thresh = varargin{1};
	else
		thresh = 0.33;
	end
	imgOut = logical(M > thresh*max(M(:)));

end
