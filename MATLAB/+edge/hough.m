%% Segmentation - Hough Transform
% Author: Connor Morales
% 
% Description:
%   This function performs a Hough transform on the input image.  The angle
%   ranges from -90 degrees to 89 degrees in single degree increments, and
%   the Rho values are set according to the image size in single pixel
%   increments.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Boolean flag, TRUE - display plot of transform
%
% Output:
%   (1) - Hough transform matrix (rho, theta)
%   (2) - Angle vector
%   (3) - Rho vector
%   (4) - Cell array of most prominent lines (pixels in [y,x])
% 

function [bins, theta, rho, vals] = hough(imgIn, showplot)

	% Type forcing
	imgIn = logical(imgIn);

	% Sizings
	[h,w] = size(imgIn);
	
	% Determine max distances possible
	D = round(sqrt(h^2 + w^2));

	% Create vectors for angle and distance
	theta = -90:89;
	rho = -D:D;
	
	% Sizings of vectors
	nd = length(rho);
	na = length(theta);
	
	% Initialize accumulation bins
	bins = zeros(nd,na);
	
	% Precompute sine (y) and cosine (x) matrices
	% Round them to nearest integer (whole pixel value)
	%  Ind 1: x/y value
	%  Ind 2: theta value
	preCos = (1 : w)' * cosd(theta);
	preSin = (1 : h)' * sind(theta);
	
	% Get list of all edge pixels
	[y,x] = find(imgIn);
	
	% Calculate rho values for each edge pixel
	rhoPix = round(preCos(x,:) + preSin(y,:));
	
	% Fill bins with count values
	for a = 1 : na
		% Update bins with histogram of values
		bins(:,a) = hist_cust(rhoPix(:,a), nd, [-D,D]);
	end
	
	
	
	% Sort bin counts, largest first
	svals = sort(bins(:), 'descend');
	
	% Get (x,y) pairs for each line we find, starting with most prominent
	vals = {};
	for l = 1 : 20 % TODO: make this an input to the function and make sure it isn't more than length(svals > 0)
		
		% Find all (rho,theta) pairs that have current
		[r,t] = find(bins == svals(l));
		
		% Go through each rho/theta pair and 
		for it = 1 : length(t)
			% Calculate rho values for all (x,y) pairs with current theta
			rval = round(x*cosd(theta(t(it))) + y*sind(theta(t(it))));
			
			% Find all (x,y) pairs whos rho value is in current rho/theta
			% pair
			ind = (rval == rho(r(it)));
			
			% Add current line to values
			vals = [vals; {[y(ind), x(ind)]}];
		end
		
	end
	
	
	if showplot
		figure('Name','Hough Transform', 'ToolBar','none');
		pcolor(theta, rho, bins)
		shading flat
		colormap jet
		colorbar
		xlim([-90,89]);
		ylim([-D,D]);
		xlabel('Angle (Degrees)');
		ylabel('Rho (Pixels)');
		title('Hough Transform');
	end
	
end


function [bins] = hist_cust(vals, num, ranges)

	% Create empty bins
	bins = zeros(num, 1);
	
	% Get minimum value
	mVal = min(ranges);
	
	% Go through all values and put into bins
	for v = 1 : length(vals)
		i = vals(v) - mVal + 1;
		bins(i) = bins(i) + 1;
	end

end
