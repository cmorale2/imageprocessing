%% Morphology Grayscale Package Demo
% Author: Connor Morales


%% Cleanup
close all
clear
clc
pause(0.1)


%% Erosion and Dilation

img = imread('../Images/circuit.tif');

se = morph.seSphere(2);

fprintf('Erosion...');
t = tic();
imgErode = morph.gray.erode(img, se);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Dilation...');
t = tic();
imgDilate= morph.gray.dilate(img,se);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Erosion and Dilation', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgErode)
title('Erosion');
subplot(1,3,3)
imshow(imgDilate)
title('Dilation');


%% Opening and Closing

img = imread('../Images/circuit.tif');

fprintf('Opening...');
t = tic();
imgOpen = morph.gray.open(img, morph.seSphere(3));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Closing...');
t = tic();
imgClose= morph.gray.close(img, morph.seSphere(5));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Erosion and Dilation', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgOpen)
title('Opening');
subplot(1,3,3)
imshow(imgClose)
title('Closing');


%% Gradient

img = imread('../Images/ct.tif');

fprintf('Gradient...');
t = tic();
imgGrad = morph.gray.gradient(img, true(3));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Gradient', 'ToolBar','none')
subplot(1,2,1)
imshow(img)
title('Original');
subplot(1,2,2)
imshow(imgGrad)
title('Gradient');


%% Top-Hat Transformation

img = imread('../Images/rice.tif');

fprintf('Top Hat...');
t = tic();
imgHat = morph.gray.tophat(img, morph.seSphere(40), 'white');
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

imgLin = intensity.contrastStretch(imgHat);

figure('Name','Top-Hat', 'ToolBar','none')
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgHat)
title('Top-Hat')
subplot(1,3,3)
imshow(imgLin)
title('Linearized')


%% Granulometry

img = imread('../Images/dowels.tif');

fprintf('Granulometry...');
t = tic();
dist = morph.gray.particleDistribution(img, [5,35]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Particle Distribution (Dowels)', 'ToolBar','none')
subplot(1,2,1)
imshow(img)
subplot(1,2,2)
plot(6:35, dist, 'b.-');
xlim([6,35]);
xlabel('Radius');
ylabel('SA Difference');
title('Dowel Particle Distribution');


%% Textural Segmentation

img1 = imread('../Images/texture1.tif');
img2 = imread('../Images/texture2.tif');

fprintf('Segmenting...');
t = tic();
imgNew1 = morph.gray.segment(img1, 30, 60);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Segmenting...');
t = tic();
imgNew2 = morph.gray.segment(img2, 30, 60);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Segmentation 1', 'ToolBar','none');
subplot(1,2,1)
imshow(img1)
title('Original');
subplot(1,2,2)
imshow(imgNew1)
title('Boundary');

figure('Name','Segmentation 2', 'ToolBar','none');
subplot(1,2,1)
imshow(img2)
title('Original');
subplot(1,2,2)
imshow(imgNew2)
title('Boundary')


%% Smoothing

img = imread('../Images/cygnus.tif');

fprintf('Smoothing...');
t = tic();
imgNew = morph.gray.smooth(img, morph.seSphere(5));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Smoothing', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Smoothed')

