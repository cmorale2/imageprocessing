%% Edge Detection Demo
% Author: Connor Morales

%% Cleanup
close all
clear
clc
pause(0.1);


%% Basic Edge Detection

img = imread('../Images/building.tif');

fprintf('Basic...');
t = tic;
imgNew = edge.basic(img);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Basic Edge Detection', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Edge');


%% Marr-Hildreth Edge Detection

img = imread('../Images/building.tif');

fprintf('Marr-Hildreth...');
t = tic();
imgNew = edge.marrhildreth(img, 4, 0.04);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Marr-Hildreth Edge Detection', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Edge');


%% Canny Edge Detection

img = imread('../Images/building.tif');

fprintf('Canny...');
t = tic;
imgNew = edge.canny(img, 4, 'scale', [0.1,  0.04]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Canny Edge Detection', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Edge');


%% Hough Transform

img = imread('../Images/airport.tif');

imgEdge = edge.canny(img, 2, 'scale', [0.15,0.05]);

fprintf('Hough...');
t = tic;
edge.hough(imgEdge, true);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

