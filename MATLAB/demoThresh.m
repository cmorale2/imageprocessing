%% Thresholding Demo
% Author: Connor Morales

%% Cleanup
close all
clear
clc
pause(0.1);


%% Basic Adaptive Thresholding

img = imread('../Images/fingerprint.tif');

fprintf('Adaptive...');
t = tic;
thresh = threshold.tAdapt(img);
imgNew = threshold.single(img, thresh);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

hist = histogram.compute(img);

figure('Name','Basic Adaptive Thresholding', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
bar(0:255, hist, 'b');
hold on
plot([thresh,thresh], [0,max(hist)], 'r-');
xlim([0,255]);
ylim([0, max(hist)]);
xlabel('Intensity Value');
ylabel('Count');
title('Histogram');
subplot(1,3,3)
imshow(imgNew);
title('Thresholded');


%% Otsu's Method

img = imread('../Images/polymersome.tif');

fprintf('Otsu...');
t = tic;
hist = histogram.compute(img);
thresh = threshold.tOtsu(histogram.normalize(hist));
imgNew = threshold.single(img, thresh);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Otsu Thresholding', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
bar(0:255, hist, 'b');
hold on
plot([thresh,thresh], [0,max(hist)], 'r-');
xlim([0,255]);
ylim([0, max(hist)]);
xlabel('Intensity Value');
ylabel('Count');
title('Histogram');
subplot(1,3,3)
imshow(imgNew);
title('Thresholded');


%% Smoothing

img = imread('../Images/large.tif');

imgSmooth = spatial.meanArithmetic(img,5);

histRaw = histogram.compute(img);
histNew = histogram.compute(imgSmooth);

fprintf('Smoothing...');
t = tic;
threshRaw = threshold.tOtsu(histogram.normalize(histRaw));
threshNew = threshold.tOtsu(histogram.normalize(histNew));
imgRaw = threshold.single(img, threshRaw);
imgNew = threshold.single(imgSmooth, threshNew);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Smoothing Thresholding', 'ToolBar','none');
subplot(2,3,1)
imshow(img)
title('Original')
subplot(2,3,2)
bar(0:255, histRaw, 'b');
hold on
plot([threshRaw,threshRaw], [0,max(histRaw)], 'r-');
xlim([0,255]);
ylim([0, max(histRaw)]);
xlabel('Intensity Value');
ylabel('Count');
title('Histogram');
subplot(2,3,3)
imshow(imgRaw);
title('Thresholded');

subplot(2,3,4)
imshow(imgSmooth)
title('Smoothed')
subplot(2,3,5)
bar(0:255, histNew, 'b');
hold on
plot([threshNew,threshNew], [0,max(histNew)], 'r-');
xlim([0,255]);
ylim([0, max(histNew)]);
xlabel('Intensity Value');
ylabel('Count');
title('Histogram');
subplot(2,3,6)
imshow(imgNew);
title('Thresholded');


%% Edge Thresholding

img = imread('../Images/small.tif');

fprintf('Edge...');
t = tic;
thresh = threshold.tEdge(img, 'magnitude', 99.7);
imgNew = threshold.single(img, thresh);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Edge Thresholding', 'ToolBar','none')
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Thresholded')


%% Double Otsu's Method

img = imread('../Images/iceburg.tif');

fprintf('Double Otsu...');
t = tic;
hist = histogram.compute(img);
thresh = threshold.tOtsuDouble(histogram.normalize(hist));
imgNew = threshold.double(img, thresh);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Double Otsu Threshold', 'ToolBar','none')
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
bar(0:255, hist, 'b');
hold on
plot(thresh(2)*ones(2,1), [0,max(hist)], 'r-');
plot(thresh(1)*ones(2,1), [0,max(hist)], 'r-');
xlim([0,255]);
ylim([0, max(hist)]);
xlabel('Intensity Value');
ylabel('Count');
title('Histogram');
subplot(1,3,3)
imshow(imgNew)
title('Thresholded')


%% Partitioning

img = imread('../Images/large_grad.tif');

fprintf('Partitioning...');
t = tic;
[imgNew, thresh, hist] = threshold.partition(img, [2,3]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Segmented Otsu - Images', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Segmented')


figure('Name','Segmented Otsu - Histograms', 'ToolBar','none');
subplot(2,3,1)
bar(0:255, hist{1,1}, 'b');
hold on
plot(thresh(1,1)*[1,1], [0,max(hist{1,1})], 'r-');
xlim([0,255]);
ylim([0,max(hist{1,1})]);

subplot(2,3,2)
bar(0:255, hist{1,2}, 'b');
hold on
plot(thresh(1,2)*[1,1], [0,max(hist{1,2})], 'r-');
xlim([0,255]);
ylim([0,max(hist{1,2})]);

subplot(2,3,3)
bar(0:255, hist{1,3}, 'b');
hold on
plot(thresh(1,3)*[1,1], [0,max(hist{1,3})], 'r-');
xlim([0,255]);
ylim([0,max(hist{1,3})]);

subplot(2,3,4)
bar(0:255, hist{2,1}, 'b');
hold on
plot(thresh(2,1)*[1,1], [0,max(hist{2,1})], 'r-');
xlim([0,255]);
ylim([0,max(hist{2,1})]);

subplot(2,3,5)
bar(0:255, hist{2,2}, 'b');
hold on
plot(thresh(2,2)*[1,1], [0,max(hist{2,2})], 'r-');
xlim([0,255]);
ylim([0,max(hist{2,2})]);

subplot(2,3,6)
bar(0:255, hist{2,3}, 'b');
hold on
plot(thresh(2,3)*[1,1], [0,max(hist{2,3})], 'r-');
xlim([0,255]);
ylim([0,max(hist{2,3})]);


%% Variable Local

img = imread('../Images/yeast.tif');

fprintf('Variable local...');
t = tic;
imgNew = threshold.local(img, 3, [30,1.5]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Variable Local', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgNew)
title('Segmented')

