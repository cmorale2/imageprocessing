%% Spatial Transformation - Utility - Sobel Kernel
% Author: Connor Morales
% 
% Description:
%   This function creates a 3x3 sobel kernel using whole numbers.  The user
%   can specify whether it is in the x (width) or y (height) direction.
% 
% Input:
%   (1) - Boolean flag, TRUE - X, FALSE -Y
%
% Output:
%   (1) - Kernel
% 

function [kern] = kernelSobel(xyn)

	kern = [ -1, -2, -1;
	          0,  0,  0;
	          1,  2,  1 ];

	if xyn
		kern = rot90(kern,-1);
	end
	
end
