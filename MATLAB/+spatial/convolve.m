%% Spatial Transformation - Convolution
% Author: Connor Morales
% 
% Description:
%   This function performs convolution on the input image and kernel.  The
%   kernel is rotated 180 degrees, then correlation is performed.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Convolution kernel (rectangular, odd dimensions)
%
% Output:
%   (1) - Convolution of input and kernel
% 

function [imgOut] = convolve(imgIn, kernel)

	imgOut = spatial.correlate(imgIn, rot90(kernel,2));
	
end
