%% Spatial Transform - Order-Statistic Filter - Alpha-Trimmed Mean
% Author: Connor Morales
% 
% Description:
%   This function performs alpha-trimmed mean filtering on the input image
%   with a window size and d-value as specified
% 
% Input:
%   (1) - Input intensity image
%   (2) - Size of window to use (single value or [width,height] vector)
%   (3) - d-value
%
% Output:
%   (1) - Filtered intensity image
% 

function [imgOut] = osAlphaTrim(imgIn, fSize, d)

	% Type forcing
	imgIn = double(imgIn);
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Window dimensioning
	[krx,kry] = internal.windowRange(fSize);
	nx = 2*max(krx) + 1;
	ny = 2*max(kry) + 1;

	% d-value sizing
	if d < 0 || d > ( nx*ny - 1 )
		error('d value (%i) must be within range 0 <= d <= mn-1 (%i)',d, nx*ny-1);
	end
	d2 = floor(d/2);
	
	% Initialize output
	imgOut = zeros(iH,iW);

	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Find range of values around current pixel
			kx = x + krx;
			ky = y + kry;
			
			% Only use values in range
			kx = kx(kx >= 1 & kx <= iW);
			ky = ky(ky >= 1 & ky <= iH);

			% Set new value
			temp = imgIn(ky,kx);
			temp = sort(temp(:), 'ascend');
			temp = temp(d2+1:length(temp)-d2);
			imgOut(y,x) = sum(temp(:)) / (nx*ny - d);
			
		end
	end
	
	% Type forcing
	imgOut = uint8(round(imgOut));

end

