%% Spatial Transform - Order-Statistic Filter - Median
% Author: Connor Morales
% 
% Description:
%   This function performs median filtering on the input image using the
%   size as specified.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Size of window to use (single value or [width,height] vector)
%
% Output:
%   (1) - Filtered intensity image
% 

function [imgOut] = osMedian(imgIn, fSize)

	% Type forcing
	imgIn = double(imgIn);
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Window dimensioning
	[krx,kry] = internal.windowRange(fSize);
	
	% Initialize output
	imgOut = zeros(iH,iW);
	
	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Find range of values around current pixel
			kx = x + krx;
			ky = y + kry;
			
			% Only use values in range
			kx = kx(kx >= 1 & kx <= iW);
			ky = ky(ky >= 1 & ky <= iH);
			
			% Calculate median value by sorting and taking middle value(s)
			% MATLAB's internal 'median' function is slow
			temp = imgIn(ky,kx);
			temp = sort(temp(:));
			len = length(temp);
			if mod(len,2) == 1
				% Odd, just take center
				imgOut(y,x) = temp(floor(len/2));
			else
				% Even, take mean of center 2
				imgOut(y,x) = ( temp(floor(len/2)) + temp(ceil(len/2)) ) / 2;
			end
			
			
		end
	end
	
	% Type forcing
	imgOut = uint8(round(imgOut));

end

