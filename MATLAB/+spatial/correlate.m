%% Spatial Transformation - Correlation
% Author: Connor Morales
% 
% Description:
%   This function correlates the input image with the kernel.  For all
%   pixels that cause the kernel to be out of bounds, the boundary pixel is
%   repeated.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Correlation kernel (rectangular, odd dimensions)
%
% Output:
%   (1) - Correlation of input and kernel
% 

function [imgOut] = correlate(imgIn, kernel)

	% Type forcing
	imgIn = double(imgIn);
	kernel = double(kernel);

	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Kernel dimensions
	[kH, kW] = size(kernel);
	
	% Check for odd
	if mod(kH,2) ~= 1 || mod(kW,2) ~= 1
		error('Kernel dimensions (W%i,H%i) must be odd', kW, kH);
	end
	
	% Initialize output
	imgOut = zeros(iH, iW);
	
	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Initialize sum-of-products to 0
			sop = 0;
			
			% Go through all kernel values
			for kx = 1 : kW
				for ky = 1 : kH
					% Calculate offset from main image
					xOff = kx - floor(kW/2) - 1;
					yOff = ky - floor(kH/2) - 1;
					
					% If offset causes out of bounds on main image, set the
					% offsets such that adding them to the image indices
					% will result in the boundary value
					% Width
					if x+xOff < 1
						xOff = 1-x;
					elseif x+xOff > iW
						xOff = iW-x;
					end
					
					% Height
					if y+yOff < 1
						yOff = 1-y;
					elseif y+yOff > iH
						yOff = iH-y;
					end
					
					% Add to SOP
					sop = sop + imgIn(y+yOff, x+xOff)*kernel(ky,kx);
				end
			end
			
			% Assign output pixel as SOP
			imgOut(y,x) = sop;
			
		end
	end
	
end
