%% Spatial Transform - Mean Filter - Contraharmonic
% Author: Connor Morales
% 
% Description:
%   This function performs contraharmonic mean filtering on the input 
%   intensity image using a window size as specified
% 
% Input:
%   (1) - Input intensity image
%   (2) - Size of window to use (single value or [width,height] vector)
%   (3) - Filter order
%
% Output:
%   (1) - Filtered intensity image
% 

function [imgOut] = meanContraharmonic(imgIn, fSize, Q)

	% Type forcing, and offset so don't divide by 0
	imgIn = double(imgIn) + 1;
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Window dimensioning
	internal.windowRange(fSize); % Just for error checking
	if length(fSize) >= 2
		kW = fSize(1);
		kH = fSize(2);
	else
		kW = fSize;
		kH = fSize;
	end
	
	% Initialize output
	imgOut = zeros(iH,iW);

	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Initialize sum-of-products to 0
			num = 0;
			den = 0;
			
			% Go through all kernel values
			for kx = 1 : kW
				for ky = 1 : kH
					% Calculate offset from main image
					xOff = kx - floor(kW/2) - 1;
					yOff = ky - floor(kH/2) - 1;
					
					% If offset causes out of bounds on main image, set the
					% offsets such that adding them to the image indices
					% will result in the boundary value
					% Width
					if x+xOff < 1
						xOff = 1-x;
					elseif x+xOff > iW
						xOff = iW-x;
					end
					
					% Height
					if y+yOff < 1
						yOff = 1-y;
					elseif y+yOff > iH
						yOff = iH-y;
					end
					
					% Add to SOP
					num = num + imgIn(y+yOff, x+xOff)^(Q+1);
					den = den + imgIn(y+yOff, x+xOff)^(Q);
				end
			end
			
			% Assign output pixel as normalized SOP
			imgOut(y,x) = num / den;
			
		end
	end
	
	% Type forcing and undo offset
	imgOut = uint8(round(imgOut - 1));

end
