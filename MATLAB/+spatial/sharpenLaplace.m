%% Spatial Transform - Sharpening Filter - Laplacian
% Author: Connor Morales
% 
% Description:
%   This function performs sharpening using the laplacian operator to find
%   and highlight quick changes in intensity.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Boolean flag, TRUE - N8 laplacian, FALSE - N4 laplacian
%   (3) - Boolean flag, TRUE - show debug images in own figure
%
% Output:
%   (1) - Filtered intensity image
% 

function [imgOut] = sharpenLaplace(imgIn, n8, debug)

	% Get kernel
	kern = spatial.kernelLaplace(n8);

	% Convolve kernel w/ image (symmetric, so only correlate)
	imgLap = double(spatial.correlate(imgIn, kern));

	% Set output image
	imgOut = double(imgIn) - imgLap;
	imgOut = uint8(round(imgOut));

	% Debug figure
	if debug
		figure('Name','Laplacian Sharpening Debug Images', 'ToolBar','none');

		subplot(1,3,1)
		imshow(imgIn);
		title('Input');

		subplot(1,3,2)
		imgShow = imgLap + min(imgLap(:));
		imgShow = imgShow * (255 / max(imgShow(:)));
		imgShow = uint8(round(imgShow));
		imshow(imgShow);
		title('Laplacian');

		subplot(1,3,3)
		imshow(imgOut);
		title('Output');
	end

end
