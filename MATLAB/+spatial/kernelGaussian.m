%% Spatial Transformation - Utility - Gaussian Kernel
% Author: Connor Morales
% 
% Description:
%   This function creates a gaussian kernel using the sigma value
%   specified.  The size of the kernel will always be square and have a
%   width that encompasses 6 standard deviations (-3sigma to 3sigma).
% 
% Input:
%   (1) - Standard deviation value (sigma)
%
% Output:
%   (1) - Kernel
%   (2) - Kernel derivative
% 

function [kern, kernDev] = kernelGaussian(sigma)

	% Function
	g = @(x) exp(-(x.^2) / (2*sigma^2) );

	% Find width we need
	a = round(3*sigma - 0.5);
	
	% Create 1D kernel
	r = (-a:a)';
	k = g(r);

	% Create 2D kernel by crossing the 1D kernel w/ itself
	kern = k * k';

	% Create 2D derivative of kernel
	kernDev = (-r .* k);
	kernDev = kernDev * kernDev';

	% Scale kernels
	kern = kern / sum(kern(:));
	kernDev = kernDev / sum(kernDev(:));

end
