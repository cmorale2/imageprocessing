%% Spatial Transform - Mean Filter - Geometric
% Author: Connor Morales
% 
% Description:
%   This function performs geometric mean filtering on the input intensity
%   image using a window size as specified
% 
% Input:
%   (1) - Input intensity image
%   (2) - Size of window to use (single value or [width,height] vector)
%
% Output:
%   (1) - Filtered intensity image
% 

function [imgOut] = meanGeometric(imgIn, fSize)

	% Type forcing, and offset so products don't get set to 0
	imgIn = double(imgIn) + 1;
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Window dimensioning
	internal.windowRange(fSize); % Just for error checking
	if length(fSize) >= 2
		kW = fSize(1);
		kH = fSize(2);
	else
		kW = fSize;
		kH = fSize;
	end
	
	% Initialize output
	imgOut = zeros(iH,iW);

	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Initialize product-of-sums to 1
			pos = 1;
			
			% Go through all kernel values
			for kx = 1 : kW
				for ky = 1 : kH
					% Calculate offset from main image
					xOff = kx - floor(kW/2) - 1;
					yOff = ky - floor(kH/2) - 1;
					
					% If offset causes out of bounds on main image, set the
					% offsets such that adding them to the image indices
					% will result in the boundary value
					% Width
					if x+xOff < 1
						xOff = 1-x;
					elseif x+xOff > iW
						xOff = iW-x;
					end
					
					% Height
					if y+yOff < 1
						yOff = 1-y;
					elseif y+yOff > iH
						yOff = iH-y;
					end
					
					% Add to SOP
					pos = pos * imgIn(y+yOff, x+xOff);
				end
			end
			
			% Assign output pixel as normalized POS
			imgOut(y,x) = pos ^ (1 / (kH*kW));
			
		end
	end
	
	% Type forcing and undo offset
	imgOut = uint8(round(imgOut - 1));

end
