%% Spatial Transform - Sharpening Filter - Highboost
% Author: Connor Morales
% 
% Description:
%   This function performs sharpening using unsharp/highboost filtering.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Highboost constant (k >= 0)
%   (3) - Type of blurring to perform
%   (4) - Data for blurring type (size/sigma)
%   (5) - Boolean flag, TRUE - show debug images in own figure
%
% Output:
%   (1) - Filtered intensity image
% 
% TODO: add other blurring types and change inputs to accomodate

function [imgOut] = sharpenHighboost(imgIn, k, type, data, debug)

	% Sanity checks
	if k < 0
		error('Highboost constant (%.1f) must be >= 0', k);
	end

	% Type forcing
	imgIn = double(imgIn);

	% Depending on type, filter image
	if strcmpi(type, 'gaussian')
		% 'data' is sigma
		[kern,~] = spatial.kernelGaussian(data);
		% Blur image
		imgBlur = spatial.correlate(imgIn, kern);
	
	elseif strcmpi(type, 'arithmetic')
		% 'data' is size
		imgBlur = spatial.meanArithmetic(imgIn, data);

	elseif strcmpi(type, 'geometric')
		% 'data' is size
		imgBlur = spatial.meanGeometric(imgIn, data);

	end

	% Subtract blur from original
	imgSub = imgIn - double(imgBlur);

	% Perform highboost
	imgOut = imgIn + imgSub;
	imgOut = uint8(round(imgOut));

	% Debug filters
	if debug

		figure('Name','Highboost Sharpening Debug Images', 'ToolBar','none');

		subplot(2,2,1);
		imshow(uint8(imgIn));
		title('Input');

		subplot(2,2,2);
		imshow(imgBlur);
		title('Blurred');

		subplot(2,2,3);
		imgSub = imgSub - min(imgSub(:));
		imgSub = imgSub * (255 / max(imgSub(:)));
		imgSub = uint8(round(imgSub));
		imshow(imgSub);
		title('Subtracted');

		subplot(2,2,4);
		imshow(imgOut);
		title('Output');

	end

end
