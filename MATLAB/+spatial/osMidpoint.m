%% Spatial Transform - Order-Statistic Filter - Midpoint
% Author: Connor Morales
% 
% Description:
%   This function performs midpoint filtering on the input image using the
%   size as specified.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Size of window to use (single value or [width,height] vector)
%
% Output:
%   (1) - Transformed intensity image
% 

function [imgOut] = osMidpoint(imgIn, fSize)

	% Type forcing
	imgIn = double(imgIn);
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Window dimensioning
	[krx,kry] = internal.windowRange(fSize);
	
	% Initialize output
	imgOut = zeros(iH,iW);
	
	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Find range of values around current pixel
			kx = x + krx;
			ky = y + kry;
			
			% Only use values in range
			kx = kx(kx >= 1 & kx <= iW);
			ky = ky(ky >= 1 & ky <= iH);

			% Set max or min value
			temp = imgIn(ky,kx);
			imgOut(y,x) = ( max(temp(:)) + min(temp(:)) ) / 2;
			
		end
	end
	
	% Type forcing
	imgOut = uint8(round(imgOut));

end

