%% Spatial Transformation - Utility - Laplacian Kernel
% Author: Connor Morales
% 
% Description:
%   This function creates a 3x3 laplacian kernel using whole numbers.  The
%   user can specify whether only N4 neighbors are used or whether all N8
%   are used.  The center value is negative.
% 
% Input:
%   (1) - Boolean flag, TRUE - N8, FALSE -N4
%
% Output:
%   (1) - Kernel
% 

function [kern] = kernelLaplace(n8n4n)

	if n8n4n
		kern = [ 1,  1, 1;
		         1, -8, 1;
		         1,  1, 1 ];
	
	else
		kern = [ 0,  1, 0;
		         1, -4, 1;
		         0,  1, 0 ];

	end

end
