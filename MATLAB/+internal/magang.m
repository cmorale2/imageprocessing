%% Utility - Magnitude and Angle Images
% Author: Connor Morales
% 
% Description:
%   This function computes the magnitude and angle images using the input
%   intensity image.  The magnitude image is computed using sobel operators
%   to perform differentiation in the X and Y direction.  The magnitude can
%   be one of two different types:
%   sum: M = |Gx| + |Gy|
%   sqrt: M = sqrt( Gx^2 + Gy^2 )
%   The angle is calculated using inverse tangent.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Optional - Type of magnitude (sum, sqrt)
%
% Output:
%   (1) - Magnitude image
%   (2) - Angle image (-180 to 180)
% 

function [varargout] = magang(imgIn, varargin)

	% Type forcing
	imgIn = double(imgIn);
	
	% Check inputs
	if nargin > 1
		if strcmpi(varargin{1}, 'sum')
			M = @(X,Y) abs(X) + abs(Y);
		elseif strcmpi(varargin{1}, 'sqrt')
			M = @(X,Y) sqrt(X.^2 + Y.^2);
		else
			error('Unknown magnitude type: %s', varargin{1});
		end
	else
		M = @(X,Y) sqrt(X.^2 + Y.^2);
	end

	% Calculate  gradients
	Gx = spatial.convolve(imgIn, spatial.kernelSobel(true));
	Gy = spatial.convolve(imgIn, spatial.kernelSobel(false));
	
	% Check outputs
	if nargout > 2
		error('Maximum outputs 2, wanted %i', nargout);
	end
	
	% Calculate magnitude
	if nargout > 0
		varargout{1} = M(Gx,Gy);
	end
	
	% Calculate angle if needed
	if nargout > 1
		varargout{2} = atan2d(Gy,Gx);
	end

end
