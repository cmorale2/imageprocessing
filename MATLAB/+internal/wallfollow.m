%% Utility - Wallfollow Clockwise
% Author: Connor Morales
% 
% Description:
%   This function performs wall following on the input image and outputs a
%   queue of pixels that are located on that wall.
%   The component is classified by the input image value at the seed.  The
%   inside wall of this component is traversed.
%   If the seed location does not lie on the wall of the component, the
%   first pixel will be the first edge found by traveling directly up (-y
%   direction)
% 
% Input:
%   (1) - Input image
%   (2) - Seed (y,x) located on inside edge of wall to follow
%
% Output:
%   (1) - QueueGeneric full of pixels on wall
% 

function [pix] = wallfollow(imgIn, seed)

	% Directions
	DIR_T = 0;
	DIR_B = 1;
	DIR_R = 2;
	DIR_L = 3;
	
	% Initialize stack of pixels
	pix = struct.QueueGeneric();

	% Sizing
	[h,w] = size(imgIn);
	
	% Current pixel
	y = seed(1);
	x = seed(2);
	val = imgIn(y,x);
	
	% Special case - single pixel component
	if    (x==1 || imgIn(y,x-1)~=val) ...
	   && (x==w || imgIn(y,x+1)~=val) ...
	   && (y==1 || imgIn(y-1,x)~=val) ...
	   && (y==h || imgIn(y+1,x)~=val)
		pix.Enqueue([y,x]);
		return;
	end
	
	% Find wall
	while true
		% If we are at edge of image, use it as wall
		if y == 1
			break;
		end
		% If the pixel above us has a different value, we are at edge of
		% component
		if y>1 && imgIn(y-1,x)~=val
			break;
		end
		y = y - 1;
	end
	
	% Add first pixel
	pix.Enqueue([y,x]);
	
	% Set new seed location
	seed = [y,x];
	
	% Point is to the right
	direction = DIR_R;
	
	% Follow wall
	while true
		
		% End condition - data in stack and @ beginning
		if pix.Count>1 && y == seed(1) && x == seed(2)
			break;
		end
		
		% Check direction
		switch direction
			case DIR_T
				% Check for rotation to left
				if x>1 && imgIn(y,x-1)==val
					direction = DIR_L;
					x = x - 1;
					pix.Enqueue([y,x]);
					
				% Check for move forward
				elseif y>1 && imgIn(y-1,x)==val
					y = y - 1;
					pix.Enqueue([y,x]);
					
				% Otherwise, rotate right
				else
					direction = DIR_R;
					
				end
			
			case DIR_B
				% Check for rotation to left
				if x<w && imgIn(y,x+1)==val
					direction = DIR_R;
					x = x + 1;
					pix.Enqueue([y,x]);
					
				% Check for move forward
				elseif y<h && imgIn(y+1,x)==val
					y = y + 1;
					pix.Enqueue([y,x]);
					
				% Otherwise, rotate right
				else
					direction = DIR_L;
					
				end
				
			case DIR_R
				% Check for rotation to left
				if y>1 && imgIn(y-1,x)==val
					direction = DIR_T;
					y = y - 1;
					pix.Enqueue([y,x]);
					
				% Check for move forward
				elseif x<w && imgIn(y,x+1)==val
					x = x + 1;
					pix.Enqueue([y,x]);
					
				% Otherwise, rotate right
				else
					direction = DIR_B;
					
				end
				
			case DIR_L
				% Check for rotation to left
				if y<h && imgIn(y+1,x)==val
					direction = DIR_B;
					y = y + 1;
					pix.Enqueue([y,x]);
					
				% Check for move forward
				elseif x>1 && imgIn(y,x-1)==val
					x = x - 1;
					pix.Enqueue([y,x]);
					
				% Otherwise, rotate right
				else
					direction = DIR_T;
					
				end
				
		end
		
	end
	
end
