%% Utility - Floodfill N4 Neighbors
% Author: Connor Morales
% 
% Description:
%   This function fills in areas of the input image with the same intensity
%   (value) that are connected with N4 connectivity.
%   Starting at the specified seed location, the output image will be
%   set to the specified value at all locations where the input image has
%   the same value as at the seed location, spreading using N4 neighbors.
%   If the output image already has a value at this location, it will be
%   skipped.
%   An empty output image will have all values 0.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Output image (will overwrite filled pixels)
%   (3) - Seed value (y,x) (2-length vector)
%   (4) - Value to fill with
%
% Output:
%   (1) - Output image
% 

function [imgOut] = floodfill4(imgIn, imgOut, seed, val)

	% Check to see if we actually need to do work
	if imgOut(seed(1), seed(2)) == val
		return;
	end

	% Image dimensions
	[h,w] = size(imgIn);
	
	% Create frontier with seed value
	frontier = struct.QueueGeneric();
	frontier.Enqueue(seed);
	
	% Get value we are interested in
	fval = imgIn(seed(1), seed(2));
	
	% Set seed pixel to value in output
	imgOut(seed(1), seed(2)) = val;
	
	% Continue until frontier is empty
	while ~frontier.isEmpty()
		
		% Get pixel
		pix = frontier.Dequeue();
		y = pix(1);
		x = pix(2);
		
		% Go through N4 neighbors and add to frontier if they have same
		% value as input image seed.  Also set to correct value in output
		
		% Right
		if x < w && imgIn(y,x+1)==fval && imgOut(y,x+1)==0
			frontier.Enqueue([y,x+1]);
			imgOut(y,x+1) = val;
		end
		
		% Top
		if y > 1 && imgIn(y-1,x)==fval && imgOut(y-1,x)==0
			frontier.Enqueue([y-1,x]);
			imgOut(y-1,x) = val;
		end
		
		% Left
		if x > 1 && imgIn(y,x-1)==fval && imgOut(y,x-1)==0
			frontier.Enqueue([y,x-1]);
			imgOut(y,x-1) = val;
		end
		
		% Bottom
		if y < h && imgIn(y+1,x)==fval && imgOut(y+1,x)==0
			frontier.Enqueue([y+1,x]);
			imgOut(y+1,x) = val;
		end
		
	end
	
end
