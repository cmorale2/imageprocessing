%% Utility - Window Ranges
% Author: Connor Morales
% 
% Description:
%   This function takes in a window size and outputs the ranges in both the
%   x and y direction.  If the input is a single value, the window will be
%   a square shape.  If the input is a vector, the first value is x (width)
%   and the second value is y (height).
% 
% Input:
%   (1) - Window size
%
% Output:
%   (1) - X (width) range
%   (2) - Y (height) range
% 

function [krx,kry] = windowRange(fSize)

	if length(fSize) >= 2
		if length(fSize) > 2
			warning('More than 2 window dimensions given (%i)', length(fSize));
		end

		if mod(fSize(1),2) ~= 1
			error('Window x size must be odd, given (%i)', fSize(1));
		end
		kCenter = floor(fSize(1)/2);
		krx = -kCenter : kCenter;

		if mod(fSize(2),2) ~= 1
			error('Window y size must be odd, given (%i)', fSize(2));
		end
		kCenter = floor(fSize(2)/2);
		kry = -kCenter : kCenter;


	else
		if mod(fSize,2) ~= 1
			error('Window size must be odd, given (%i)', fSize);
		end
		kCenter = floor(fSize/2);
		krx = -kCenter : kCenter;
		kry = krx;
	end

end
