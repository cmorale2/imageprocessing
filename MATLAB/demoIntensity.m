%% Intensity Package Demo
% Author: Connor Morales


%% Cleanup
close all
clear
clc
pause(0.1)


%% Image Negative

img = imread('../Images/mammogram.tif');

fprintf('Negative...');
t = tic();
imgNew = intensity.negative(img);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Image Negative', 'ToolBar','none');
subplot(1,2,1);
imshow(img);
title('Original');
subplot(1,2,2);
imshow(imgNew);
title('Negative');

pause(0.1);



%% Log Transform

img = imread('../Images/spectrum.tif');

fprintf('Log...');
t = tic();
imgNew = intensity.log(img);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Image Log Transform', 'ToolBar','none');
subplot(1,2,1);
imshow(img);
title('Original');
subplot(1,2,2);
imshow(imgNew);
title('Transform');

pause(0.1);



%% Gamma Transform < 1

img = imread('../Images/spine.tif');

imgNew = cell(3,1);
fprintf('Gamma...');
t = tic();
imgNew{1} = intensity.gamma(img, 0.6);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Gamma...');
t = tic();
imgNew{2} = intensity.gamma(img, 0.4);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Gamma...');
t = tic();
imgNew{3} = intensity.gamma(img, 0.3);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Image Gamma Transform < 1', 'ToolBar','none');
subplot(2,2,1);
imshow(img);
title('Original');
subplot(2,2,2);
imshow(imgNew{1});
title('\gamma = 0.6');
subplot(2,2,3);
imshow(imgNew{2});
title('\gamma = 0.4');
subplot(2,2,4);
imshow(imgNew{3});
title('\gamma = 0.3');

pause(0.1);



%% Gamma Transform > 1

img = imread('../Images/aerial.tif');

imgNew = cell(3,1);
fprintf('Gamma...');
t = tic();
imgNew{1} = intensity.gamma(img, 3);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Gamma...');
t = tic();
imgNew{2} = intensity.gamma(img, 4);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Gamma...');
t = tic();
imgNew{3} = intensity.gamma(img, 5);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Image Gamma Transform > 1', 'ToolBar','none');
subplot(2,2,1);
imshow(img);
title('Original');
subplot(2,2,2);
imshow(imgNew{1});
title('\gamma = 3.0');
subplot(2,2,3);
imshow(imgNew{2});
title('\gamma = 4.0');
subplot(2,2,4);
imshow(imgNew{3});
title('\gamma = 5.0');

pause(0.1);


%% Contrast Stretching

img = imread('../Images/pollen_washout.tif');

hist = histogram.compute(img);

fprintf('Contrast Stretch...');
t = tic();
imgNew = intensity.contrastStretch(img);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

histNew = histogram.compute(imgNew);

figure('Name','Contrast Stretching', 'ToolBar','none');
subplot(2,2,1)
imshow(img);
title('Original');
subplot(2,2,2)
imshow(imgNew);
title('Stretched');
subplot(2,2,3)
bar(0:255, hist, 'b');
xlim([0,255]);
subplot(2,2,4)
bar(0:255, histNew, 'b');
xlim([0,255]);


pause(0.1);


%% Intensity Level Slicing

img = imread('../Images/kidney.tif');

fprintf('Level Slice Binary...');
t = tic();
imgBin = intensity.levelSlice(img, 'binary', [150,255], [0, 255], false);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Level Slice Additive...');
t = tic();
imgAdd = intensity.levelSlice(img, 'additive', [40,130], 20, false);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Level Slicing', 'ToolBar','none');
subplot(1,3,1)
imshow(img);
title('Original');
subplot(1,3,2)
imshow(imgBin)
title('Binary')
subplot(1,3,3)
imshow(imgAdd)
title('Additive')

pause(0.1);
