%% Spatial Package Demo - Sharpening
% Author: Connor Morales

% TODO: update to show different options for sharpening types

%% Cleanup
close all
clear
clc
pause(0.1)


%% Image Loading

img = imread('../Images/moon.tif');


%% Laplacian Sharpening

fprintf('Laplacian...');
t = tic();
imgLap = spatial.sharpenLaplace(img, true, false);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);


%% Highboost Filtering

fprintf('Highboost...');
t = tic();
imgBoost = spatial.sharpenHighboost(img, 1, 'gaussian', 3, false);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);


%% Display

figure('Name','Sharpening');
subplot(1,3,1)
imshow(img);
title('Original');
subplot(1,3,2)
imshow(imgLap)
title('Laplacian');
subplot(1,3,3)
imshow(imgBoost)
title('Highboost')

