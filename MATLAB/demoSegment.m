%% Segmentation Demo
% Author: Connor Morales


%% Cleanup
close all
clear
clc
pause(0.1)


%% Region Growing

img = imread('../Images/weld.tif');

[y,x] = find(img == 255);
seeds = [y,x];

imgSeed = zeros(size(img));
for i = 1 : length(x)
	imgSeed(y(i), x(i)) = 1;
end

fprintf('Region Growing...');
t = tic;
imgNew = segment.rgrow(img, seeds, 68);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Region Growing', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgSeed)
title('Seeds')
subplot(1,3,3)
imshow(imgNew)
title('Final Regions')


%% Region Splitting and Dividing

img = imread('../Images/cygnus.tif');

fprintf('Region Splitting...');
t = tic;
imgNew = segment.rsplit(img, 16, [0,25, 100,255]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Region Dividing...');
imgTes = segment.rdivide(img, 16, [25,Inf, 0,125]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Splitting and Dividing', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgNew)
title('Split')
subplot(1,3,3)
imshow(imgTes)
title('Divided');


%% Watershed

img = imread('../Images/basic4.tif');

imgEdge = internal.magang(img);
imgEdge = imgEdge - min(imgEdge(:));
imgEdge = (255/max(imgEdge(:))) * imgEdge;
imgEdge = uint8(round(imgEdge));

fprintf('Watershed...');
t = tic;
[imgEdge, ~] = segment.watershedBrute(imgEdge);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Watershed', 'ToolBar','none');
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgEdge)
title('Edges')


%% Watershed Markers

img = imread('../Images/blobs.tif');


imgSmooth = spatial.correlate(img, spatial.kernelGaussian(2));
imgEdge = internal.magang(imgSmooth);
imgEdge = imgEdge - min(imgEdge(:));
imgEdge = (255/max(imgEdge(:))) * imgEdge;
imgEdge = uint8(round(imgEdge));


seed = [ 54, 22;
         22, 38;
         39, 90;
         95, 73;
        120,118;
          9,124;
         84, 32 ];

fprintf('Watershed Markers...');
t = tic;
imgLabel = segment.watershedmarkPFlood(imgEdge, seed);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

imgShow = double(imgLabel) - double(min(imgLabel(:)));
imgShow = (255/max(imgShow(:))) * imgShow;
imgShow = uint8(round(imgShow));

figure('Name','Watershed with Markers', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgEdge)
title('Magnitude')
subplot(1,3,3)
imshow(imgShow)
title('Labeled')

