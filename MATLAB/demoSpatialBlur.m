%% Spatial Package Demo - Blurring
% Author: Connor Morales

% TODO: update to show different options for each filtering type

%% Cleanup
close all
clear
clc
pause(0.1)


%% Load Noisy Images

imgGauss = imread('../Images/gauss.tif');
imgSalt = imread('../Images/salt.tif');
imgPepp = imread('../Images/pepper.tif');
imgSape = imread('../Images/saltpepper.tif');
imgUnif = imread('../Images/saltpepperuniform.tif');


%% Filter Images

fprintf('Arithmetic...');
t = tic();
gaussArith = spatial.meanArithmetic(imgGauss, 3);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Geometric...');
t = tic();
gaussGeom  = spatial.meanGeometric(imgGauss, 3);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Contraharmonic Salt...');
t = tic();
saltContra = spatial.meanContraharmonic(imgSalt, 3, -1.5);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Contraharmonic Pepper...');
t = tic();
peppContra = spatial.meanContraharmonic(imgPepp, 3, 1.5);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Median...');
t = tic();
sapeMedian = spatial.osMedian(imgSape, 5);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Min...');
t = tic();
saltMin = spatial.osMaxMin(imgSalt, 3, false);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Max...');
t = tic();
peppMax = spatial.osMaxMin(imgPepp, 3, true);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Alpha Trim...');
t = tic();
unifAlpha = spatial.osAlphaTrim(imgUnif, 5, 5);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);


%% Display Images

figure('Name','Guassian Noise Filter', 'ToolBar','none');
subplot(1,3,1)
imshow(imgGauss)
title('Original');
subplot(1,3,2)
imshow(gaussArith)
title('Arithmetic');
subplot(1,3,3)
imshow(gaussGeom);
title('Geometric');


figure('Name','Salt Noise Filter', 'ToolBar','none');
subplot(1,3,1);
imshow(imgSalt);
title('Original');
subplot(1,3,2)
imshow(saltContra);
title('Contraharmonic');
subplot(1,3,3)
imshow(saltMin)
title('Min');


figure('Name','Pepper Noise Filter', 'ToolBar','none');
subplot(1,3,1);
imshow(imgPepp);
title('Original');
subplot(1,3,2)
imshow(peppContra);
title('Contraharmonic');
subplot(1,3,3)
imshow(peppMax)
title('Max');


figure('Name','Salt and Pepper Noise Filter', 'ToolBar','none');
subplot(1,2,1)
imshow(imgSape)
title('Original');
subplot(1,2,2)
imshow(sapeMedian)
title('Median');


figure('Name','Uniform and S-n-P Noise Filter', 'ToolBar','none');
subplot(1,2,1)
imshow(imgUnif)
title('Original');
subplot(1,2,2)
imshow(unifAlpha)
title('Alpha-Trimmed Mean');

