%% Intensity Transform - Piecewise
% Author: Connor Morales
% 
% Description:
%   This function implements a piecewise intensity transformation on
%   the input image.  It uses the points provided to construct the
%   piecewise function.  If the points provided to not include the lowest
%   and highest values, they will be added by linear interpolating to the
%   points that exist.
%   If debug is set, then a plot of the mapping will be displayed in its 
%   own figure.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Matrix of points (nx2 where n >= 2)
%   (3) - Boolean flag for whether or not to display plot of mapping
%
% Output:
%   (1) - Transformed image
% 

function [imgOut] = piecewise(imgIn, points, debug)

	% Type forcing
	imgIn = double(imgIn);
	
	% Add beginning and end points if they don't exist
	if points(1,1) > 0
		points = [ 0,0; points ];
	end
	if points(size(points,1), 1) < 255
		points = [ points; 255,255 ];
	end
	
	% Create mapping from r to s
	sMap = zeros(256,1);
	for i = 2 : size(points,1);
		x = [points(i-1,1), points(i,1)];
		y = [points(i-1,2), points(i,2)];
		sMap(x(1)+1:x(2)+1) = linear_interp(x(1), x(2), y(1), y(2));
	end
	
	% Go through all points and set output image
	imgOut = zeros(size(imgIn));
	for i = 0 : 255
		imgOut(imgIn == i) = sMap(i+1);
	end
	
	imgOut = uint8(round(imgOut));
	
	if debug
		figure('Name', 'Piecewise Transform - Intensity Mapping');
		plot(0:255, sMap);
		xlim([0,255]);
		ylim([0,255]);
		xlabel('Input Image (r)');
		ylabel('Output Image (s)');
		title('Input to Output Intensity Mapping');
	end
	
end


% Inclusive of both x1 and x2
function [line] = linear_interp(x1,x2, y1,y2)
	m = (y2-y1) / (x2-x1);
	x = x1:1:x2;
	line = y1 + m*(x-x1);
end
