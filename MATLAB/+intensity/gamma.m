%% Intensity Transform - Gamma
% Author: Connor Morales
% 
% Description:
%   This function applies a gamma transform to the input image.
%   The gamma value should be greater than 0.  A value of 1 is an identity
%   transform.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Gamma value
%
% Output:
%   (1) - Output intensity image
% 

function [imgOut] = gamma(imgIn, gamma)

	imgIn = double(imgIn);
	
	gamma = double(gamma);

	imgOut = (255/(256^gamma)) * (imgIn .^ gamma);
	
	imgOut = uint8(round(imgOut));

end
