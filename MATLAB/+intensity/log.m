%% Intensity Transform - Log
% Author: Connor Morales
% 
% Description:
%   This function applies a natural log transform to the input image.
% 
% Input:
%   (1) - Input intensity image
%
% Output:
%   (1) - Output intensity image
% 

function [imgOut] = log(imgIn)

	imgIn = double(imgIn);

	imgOut = (255/log(256)) * log(1 + imgIn);
	
	imgOut = uint8(round(imgOut));

end
