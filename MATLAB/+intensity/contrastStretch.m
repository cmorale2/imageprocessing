%% Intensity Transform - Contrast Stretching
% Author: Connor Morales
% 
% Description:
%   This function performs contrast stretching on the input image.  If no
%   points are provided, a linear stretch is applied, which maps the
%   intensity range of the image to the entire intensity spectrum.  If
%   points are provided, they are used instead.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Optional - Points (nx2 where n>=2)
%
% Output:
%   (1) - Output intensity image
% 

function [imgOut] = contrastStretch(varargin)

	if ( nargin < 1 )
		error('Missing argument: input intensity image');
	end
	imgIn = double(varargin{1});
	
	if ( nargin < 2 )
		% If not given set of points, just do a linear stretch
		points = [min(imgIn(:)), 0;
		          max(imgIn(:)), 255 ];
	else
		% If given set of points, check them all
		points = varargin{2};
		if size(points, 1) < 2
			error('Must provide at least 2 points');
		end
		for p = 2 : length(points)
			% Check for bounds
			if points(p,1) < 0 || points(p,1) > 255
				error('Bounds: r%i (%i)', p, points(p,1));
			end
			if points(p,2) < 0 || points(p,2) > 255
				error('Bounds: s%i (%i)', p, points(p,2));
			end
			% Check for monotonic increase
			if points(p,1) < points(p-1,1)
				error('Monotonic: r%i (%i) < r%i (%i)', p, points(p,1), p-1, points(p-1,1));
			end
			if points(p,2) < points(p-1,2)
				error('Monotonic: s%i (%i) < s%i (%i)', p, points(p,2), p-1, points(p-1,2));
			end
		end
	end
	
	imgOut = intensity.piecewise(imgIn, points, false);

end
