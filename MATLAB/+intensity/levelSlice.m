%% Intensity Transform - Level Slicing
% Author: Connor Morales
% 
% Description:
%   This function performs level slicing on the input intensity image.  The
%   range of intensities to be highlighted is specified by range, and the
%   level to which they are highlighted is specified by level.  There are
%   two types available:
%     'binary' - Acts like thresholding in that the only two intensities
%                are those specified by level.  There must be two levels
%                specified.
%     'additive' - The highlighting is added on top of the current
%                  intensities of the image.  The range specified will be
%                  increased to the value specified by level.  Everything
%                  else will be unchanged.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Intensity range to highlight (2-long vector)
%   (3) - Level to highlight range to.  If 'binary' type, 2 levels
%   (4) - Type of slicing to perform.  See description
%   (5) - Boolean flag for whether or not to display plot of mapping
%
% Output:
%   (1) - Output intensity image
% 

function [imgOut] = levelSlice(imgIn, type, range, level, debug)

	% Input checks 
	if length(range) ~= 2
		error('Sizing: range should be size 2 not (%i)', length(range));
	end
	
	imgIn = double(imgIn);
	
	
	
	% Binary level slice - only two values in output image
	if strcmpi(type, 'binary')
		
		% Check inputs
		if length(level) ~= 2
			error('Sizing: level should be size 2 not (%i)', length(level));
		end
		
		% Create points for intensityPiecewise
		% Include endpoints because intensityPiecewise will linearly
		% interpolate otherwise
		points = [ 0, level(1)
		          range(1), level(1);
				  range(1), level(2);
				  range(2), level(2);
				  range(2), level(1); 
		          255, level(1) ];
		
		
	% Additive level slice - add to existing intensities
	elseif strcmpi(type, 'additive')
		
		% Create points for intensity piecewise
		% No need to include endpoints because intensityPiecewise will
		% linearly interpolate
		points = [ range(1), range(1);
		           range(1), level;
		           range(2), level;
		           range(2), range(2) ];
		
		
	else
		error('Bad type: %s', type);
	end

	
	imgOut = intensity.piecewise(imgIn, points, debug);

end
