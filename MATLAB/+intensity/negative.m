%% Intensity Transform - Negative
% Author: Connor Morales
% 
% Description:
%   This function applies a negative intensity transform to the input image
%   and returns the transformed image.
% 
% Input:
%   (1) - Input intensity image
%
% Output:
%   (1) - Output intensity image
% 

function [imgOut] = negative(imgIn)

	imgOut = uint8(255 - imgIn);

end
