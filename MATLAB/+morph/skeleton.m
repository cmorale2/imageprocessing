%% Morphological Transformation - Skeleton
% Author: Connor Morales
% 
% Description:
%   This function extracts a skeleton from the input image.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Optional - Maximum number of iterations to perform (default 1000)
%
% Output (Optional):
%   (1) - Binary image of skeleton
%   (2) - Boolean flag, TRUE - hit max iteration, FALSE - not
% 

function [varargout] = skeleton(imgIn, varargin)

	% Create SE we will use
	se = true(3);
	
	%  Type forcing
	imgIn = logical(imgIn);
	
	% Check max interations
	if nargin > 1
		iters = varargin{1};
	else
		iters = 1000;
	end
	
	% Loop through our erosion process
	scum = false(size(imgIn));
	for i = 0 : iters
		
		% kth erosion of input image
		temp1 = imgIn;
		for ii = 1 : i
			temp1 = morph.erode(temp1, se);
		end
		
		% Break condition is if this is 0
		if sum(uint64(temp1)) == 0
			break;
		end
		
		% Calculate cumulative subset
		temp2 = morph.open(temp1, se);
		scum = scum | (temp1 & ~temp2);
		
	end
	
	% Output final image
	if nargout > 0
		varargout{1} = logical(scum);
	end
	
	% Output iteration flag
	if nargout > 1
		varargout{2} = (i == iters);
	end
	
end
