%% Morphological Transformation - SE Sphere Creation
% Author: Connor Morales
% 
% Description:
%   This function creates a spherical structuring element.
% 
% Input:
%   (1) - Radius of sphere
%
% Output:
%   (1) - Structuring element
% 

function [se] = seSphere(r)

	% Grid creation for equations
	[x,y] = meshgrid(-r:r,-r:r);
	
	% SE creation
	se = ( x.^2 + y.^2 <= r^2 );
	
end
