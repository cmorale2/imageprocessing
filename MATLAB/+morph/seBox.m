%% Morphological Transformation - SE Box Creation
% Author: Connor Morales
% 
% Description:
%   This function creates a rectangular structuring element.
% 
% Input:
%   (1) - Height of box (y) (odd)
%   (2) - Width of box (x) (odd)
%
% Output:
%   (1) - Structuring element
% 

function [se] = seBox(h,w)

	if mod(h,2) ~= 1 || mod(w,2) ~= 1
		error('Height and Width must be odd (H%i,W%i)', h,w);
	end

	se = true(h,w);
	
end
