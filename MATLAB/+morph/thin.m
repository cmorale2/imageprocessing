%% Morphological Transformation - Thinning
% Author: Connor Morales
% 
% Description:
%   This function performs morphological thinning on the input image.
% 
% Input:
%   (1) - Input binary (logical) image
%
% Output:
%   (1) - Thinned input
% 

function [imgOut] = thin(imgIn)

	% Create set of SE we will use
	B = cell(8,1);
	
	B{1} = [ 0, 0, 0;
	        -1, 1,-1;
	         1, 1, 1 ];
	B{2} = [-1, 0, 0;
	         1, 1, 0;
	         1, 1,-1 ];
	B{3} = [ 1,-1, 0;
	         1, 1, 0;
	         1,-1, 0 ];
	B{4} = [ 1, 1,-1;
	         1, 1, 0;
	        -1, 0, 0 ];
	B{5} = [ 1, 1, 1;
	        -1, 1,-1;
	         0, 0, 0 ];
	B{6} = [-1, 1, 1;
	         0, 1, 1;
	         0, 0,-1 ];
	B{7} = [ 0,-1, 1;
	         0, 1, 1;
	         0,-1, 1 ];
	B{8} = [ 0, 0,-1;
	         0, 1, 1;
	        -1, 1, 1 ];

	
	% Input image init
	imgIn = logical(imgIn);
	
	% Initialize temp images
	imgCur = imgIn;
	
	% Loop through our thinning process until no change is detected
	while ( true )
		
		for i = 1 : length(B)
			imgOld = imgCur;
			imgCur = imgOld & ~(morph.hitormiss(imgOld, B{i}, false));
		end
		
		if isequal(imgCur, imgOld)
			break;
		end
		
	end
	
	% Output final image
	imgOut = imgCur;
	
end
