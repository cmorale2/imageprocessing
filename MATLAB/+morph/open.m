%% Morphological Transformation - Opening
% Author: Connor Morales
% 
% Description:
%   This functin performs morphological opening on the input image using
%   the structuring elemnt provided.  Opening is an erosion followed by a
%   dilation.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Structural element (odd dimensions)
%
% Output:
%   (1) - Opening of input by SE
% 

function [imgOut] = open(imgIn, se)

	imgOut = morph.dilate(morph.erode(imgIn,se), se);

end
