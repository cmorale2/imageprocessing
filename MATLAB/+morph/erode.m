%% Morphological Transformation - Erosion
% Author: Connor Morales
% 
% Description:
%   This function performs morphological erosion on the input image using
%   the structural element provided.  For the SE, a value of '1' is TRUE,
%   meaning it must overlap with a '1' on the image.  A value of '0' is
%   FALSE, meaning it must overlap with a '0' on the image.  A value of '-1'
%   is DONTCARE, meaning it can overlap with either '1' or '0'.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Structural element (odd dimensions)
%
% Output:
%   (1) - Erosion of input by SE
% 

function [imgOut] = erode(imgIn, se)

	% Type forcing
	imgIn = logical(imgIn);
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Structural element size
	[sH, sW] = size(se);
	
	% Check for odd
	if mod(sH,2) ~= 1 || mod(sW,2) ~= 1
		error('SE dimensions (W%i,H%i) must be odd', sW, sH);
	end
	
	% Initialize output
	imgOut = false(iH, iW);
	
	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Initialize output value
			curout = true;
			
			% Go through all SE
			for sx = 1 : sW
				for sy = 1 : sH
					
					% Image offset value
					rx = x + sx - floor(sW/2) - 1;
					ry = y + sy - floor(sH/2) - 1;
					
					% If we are out of bounds but the SE is TRUE,
					% the output must be FALSE
					if rx < 1 || rx > iW || ry < 1 || ry > iH
						if se(sy,sx) == 1
							curout = false;
						end
						break;
					end
					
					% If SE is TRUE, so must image be
					if se(sy,sx) == 1
						curout = (imgIn(ry,rx) == 1);
					
					% If SE is FALSE, so must image be
					elseif se(sy,sx) == 0
						curout = (imgIn(ry,rx) == 0);
						
					% If SE is DONTCARE, image doesnt matter
					else
						curout = true;
					end
					
					% Break condition
					if ~curout
						break;
					end
					
				end
				
				% Early break condition
				if ~curout
					break;
				end
				
			end
			
			% Assign output
			imgOut(y,x) = curout;
			
		end
	end
	
	% Type forcing
	imgOut = logical(imgOut);
	
end
