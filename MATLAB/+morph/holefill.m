%% Morphological Transformation - Hole Filling
% Author: Connor Morales
% 
% Description:
%   This function performs morphological hole filling on the input image
%   where the point(s) given as seed(s) is/are located in the hole(s) to
%   be filled.  For efficiency, the seed(s) should be in the middle of the
%   hole(s).
%   An optional input is the size of the SE sphere to use.  Larger spheres
%   will result in a quicker execution, but if they are too large they will
%   result in more than the hole being filled in, especially if the
%   boundary of the hole is thin.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Seed(s) of holes to fill (y,x) (size nx2)
%   (3) - Optional - Radius of SE sphere (default 1)
%
% Output:
%   (1) - Binary image of hole
% 

function [imgOut] = holefill(imgIn, seed, varargin)

	% Create SE to use
	if nargin > 2
		se = morph.seSphere(varargin{1});
	else
		se = morph.seSphere(1);
	end

	% Input image init
	imgIn = logical(imgIn);
	imgC = ~imgIn;
	
	% Sizing
	[h,w] = size(imgIn);
	
	% Initialize our image
	imgX = false(size(imgIn));
	for i = 1 : size(seed,1)
		if seed(i,1) < 0 || seed(i,1) > h || seed(i,2) < 0 || seed(i,2) > w
			error('Seed %i (%i,%i) exceeds image bounds',i,seed(i,1),seed(i,2));
		end
		imgX(seed(i,1), seed(i,2)) = true;
	end
	
	% Loop through our erosion process
	while (true)
		
		% Erode and intersection
		imgNew = morph.dilate(imgX, se) & imgC;
		
		% Check break condition
		if isequal(imgX, imgNew)
			break;
		end
		
		% Shift working image
		imgX = imgNew;
		
	end
	
	% Output final image
	imgOut = imgNew;
	
end
