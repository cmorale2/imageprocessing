%% Morphological Transformation - Boundary Extraction
% Author: Connor Morales
% 
% Description:
%   This function performs morphological boundary extraction on the input
%   image.  The erosion of the image is subtracted from the original image.
% 
% Input:
%   (1) - Input binary (logical) image
%
% Output:
%   (1) - Boundary of input
% 

function [imgOut] = boundary(imgIn)

	imgOut = logical(imgIn) & ~morph.erode(imgIn, morph.seBox(3,3));

end
