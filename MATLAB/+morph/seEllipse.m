%% Morphological Transformation - SE Ellipse Creation
% Author: Connor Morales
% 
% Description:
%   This function creates an elliptical structuring element.
% 
% Input:
%   (1) - Width radius (x)
%   (2) - Height radius (y)
%
% Output:
%   (1) - Structuring element
% 

function [se] = seEllipse(rx,ry)

	% Grid creation
	[x,y] = meshgrid(-rx:rx, -ry:ry);

	% Create SE
	se = ( (x.^2)/(rx^2) + (y.^2)/(ry^2) <= 1 );
	
end
