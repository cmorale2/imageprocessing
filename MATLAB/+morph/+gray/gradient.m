%% Morphological Transformation - Grayscale - Gradient
% Author: Connor Morales
% 
% Description:
%   This function calculates the gradient of the input image using grascale
%   morphology with the provided structuring element.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Flat structural element (odd dimensions)
%
% Output:
%   (1) - Gradient of input by SE
% 

function [imgOut] = gradient(imgIn, se)

	imgOut = morph.gray.dilate(imgIn,se) - morph.gray.erode(imgIn,se);
	
end
