%% Morphological Transformation - Grayscale - Closing
% Author: Connor Morales
% 
% Description:
%   This function performs morphological grayscale closing on the input
%   image with the structuring element provided.  Closing is dilation
%   followed by erosion.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Flat structural element (odd dimensions)
%
% Output:
%   (1) - Closing of input by SE
% 

function [imgOut] = close(imgIn, se)

	imgOut = morph.gray.erode(morph.gray.dilate(imgIn,se), se);
	
end
