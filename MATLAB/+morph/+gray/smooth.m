%% Morphological Transformation - Grayscale - Smoothing
% Author: Connor Morales
% 
% Description:
%   This function performs grayscale morphological smoothing on the input
%   image by opening it then closing it with the structural element
%   provided.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Flat structural element (odd dimensions)
%
% Output:
%   (1) - Smoothing of input by SE
% 

function [imgOut] = smooth(imgIn, se)

	imgOut = morph.gray.close(morph.gray.open(imgIn,se), se);
	
end
