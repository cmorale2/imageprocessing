%% Morphological Transformation - Grayscale - Textural Segmentation
% Author: Connor Morales
% 
% Description:
%   This function performs textural segmenetation on the input image.  For
%   images where there are two textures composed of round objects, this
%   function will output a gradient representing the boundary between
%   textures.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Radius slightly larger than small objects
%   (3) - Radius slightly larger than separation of large objects
%
% Output:
%   (1) - Boundary image
% 

function [imgOut] = segment(imgIn, small, large)

	% Close w/ small SE
	imgTemp = morph.gray.close(imgIn, morph.seSphere(small));
	
	% Open w/ large SE
	imgTemp = morph.gray.open(imgTemp, morph.seSphere(large));
	
	% Gradient
	imgOut = morph.gray.gradient(imgTemp, ones(3));

end
