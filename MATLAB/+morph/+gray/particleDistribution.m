%% Morphological Transformation - Grayscale - Granulometry
% Author: Connor Morales
% 
% Description:
%   This function performs granulometry on the input image using the range
%   specified.  The output distribution is inverted so that the most common
%   sizes will be maximum values.
%   Granulometry is used to determine the sizes of objects in an image
%   (spheres in this case).  By opening the image with SE spheres of
%   increasing size and finding the difference in surface area between
%   successive images, the sizes of objects in the image can be discovered.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Lower and upper ranges to test (2-long vector)
%
% Output:
%   (1) - Distribution
% 

function [dist] = particleDistribution(imgIn, rrange)

	% Type forcing
	imgCur = uint8(imgIn);
	
	% Initialize surface area array
	sa = zeros(rrange(2)-rrange(1)+1, 1);
	
	% Go through all radii
	for i = rrange(1) : rrange(2)
		% Create current image
		imgCur = morph.gray.open(imgCur, morph.seSphere(i));
		
		% Calculate surface area
		sa(i-rrange(1)+1) = sum(imgCur(:));
	end
	
	% Calculate distribution
	dist = -1*diff(sa);

end
