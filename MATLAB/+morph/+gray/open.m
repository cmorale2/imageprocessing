%% Morphological Transformation - Grayscale - Opening
% Author: Connor Morales
% 
% Description:
%   This function performs morphological grayscale opening on the input
%   image with the structuring element provided.  Opening is erosion
%   followed by dilation.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Flat structural element (odd dimensions)
%
% Output:
%   (1) - Opening of input by SE
% 

function [imgOut] = open(imgIn, se)

	imgOut = morph.gray.dilate(morph.gray.erode(imgIn,se), se);
	
end
