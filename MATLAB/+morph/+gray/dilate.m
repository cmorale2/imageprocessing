%% Morphological Transformation - Grayscale - Dilation
% Author: Connor Morales
% 
% Description:
%   This function performs morphological grayscale dilation on the input
%   iamge with the flat structuring element provided.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Flat structural element (odd dimensions)
%
% Output:
%   (1) - Dilation of input by SE
% 

function [imgOut] = dilate(imgIn, se)

	% Type forcing
	imgIn = uint8(imgIn);
	se = logical(se > 0);
	
	% Rotate SE for dilation
	se = rot90(se,2);
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Structural element size
	[sH, sW] = size(se);
	
	% Check for odd
	if mod(sH,2) ~= 1 || mod(sW,2) ~= 1
		error('SE dimensions (W%i,H%i) must be odd', sW, sH);
	end
	
	% Get range for SE
	srx = -floor(sW/2) : floor(sW/2);
	sry = -floor(sH/2) : floor(sH/2);
	
	% Initialize output
	imgOut = zeros(iH, iW);
	
	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Get range of pixels around current
			rx = x + srx;
			ry = y + sry;
			
			% Get list of values in range
			indx = rx>0 & rx<=iW;
			indy = ry>0 & ry<=iH;
			
			% Only use values in range
			rx = rx(indx);
			ry = ry(indy);
			
			% Get values of image
			vals = imgIn(ry,rx);
			
			% Only use values that are 1 in SE
			% and take MIN
			imgOut(y,x) = max(vals(se(indy,indx)));
			
		end
	end
	
	% Type forcing
	imgOut = uint8(imgOut);
	
end
