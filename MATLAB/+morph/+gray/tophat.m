%% Morphological Transformation - Grayscale - Top-/Bottom-Hat Transforms
% Author: Connor Morales
% 
% Description:
%   This function performs top- or bottom-hat grayscale morphological
%   transformations on the input image with the structuring element
%   provided.
%   One use is to remove objects from an image that don't fit the SE.
%   Another use is to correct nonuniform illumination.where the SE is
%   larger than any objects in the image.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Flat structural element (odd dimensions)
%   (3) - Type of transform:
%          'white' - Top-Hat transform, for light obj. on dark bg
%          'black' - Bottom-Hat transform, for dark obj. on light bg
%
% Output:
%   (1) - Transformed input
% 

function [imgOut] = tophat(imgIn, se, type)

	% Type forcing
	imgIn = uint8(imgIn);

	% Perform transform
	if strcmpi(type, 'white')
		imgOut = imgIn - morph.gray.open(imgIn,se);
	elseif strcmpi(type, 'black')
		imgOut = morph.gray.close(imgIn,se) - imgIn;
	else
		error('Unknown type: %s', type);0
	end

end
