%% Morphological Transformation - Dilation
% Author: Connor Morales
% 
% Description:
%   This function performs morphological dilation on the input image using
%   the structural element provided.  The structuring element is converted
%   to a logical matrix where '1' is TRUE and all else is FALSE.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Structural element (odd dimensions)
%
% Output:
%   (1) - Dilation of input by SE
% 

function [imgOut] = dilate(imgIn, se)

	% Type forcing
	imgIn = logical(imgIn);
	
	% We only care about a single TRUE overlap, so convert SE to ignore
	% DONTCARE values, if any exist
	se = logical(se == 1);
	
	% Image dimensions
	[iH, iW] = size(imgIn);
	
	% Rotate SE for dilation
	se = rot90(se,2);
	
	% Structural element size
	[sH, sW] = size(se);
	
	% Check for odd
	if mod(sH,2) ~= 1 || mod(sW,2) ~= 1
		error('SE dimensions (W%i,H%i) must be odd', sW, sH);
	end
	
	% Get range for SE
	srx = -floor(sW/2) : floor(sW/2);
	sry = -floor(sH/2) : floor(sH/2);
	
	% Initialize output
	imgOut = false(iH, iW);
	
	% Go through all image pixels
	for x = 1 : iW
		for y = 1 : iH
			
			% Get range of pixels around current
			rx = x + srx;
			ry = y + sry;
			
			% Get list of values in range
			indx = rx>0 & rx<=iW;
			indy = ry>0 & ry<=iH;
			
			% Only use values in range
			rx = rx(indx);
			ry = ry(indy);
			
			% AND image with SE
			seTemp = se(indy,indx);
			chk = imgIn(ry,rx) & seTemp;
			
			% Output is TRUE if any elements of img&SE are TRUE
			imgOut(y,x) = logical(sum(chk(:)));
			
		end
	end
	
	% Type forcing
	imgOut = logical(imgOut);
	
end
