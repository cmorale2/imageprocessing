%% Morphological Transformation - Hit or Miss
% Author: Connor Morales
% 
% Description:
%   This function performs the hit-or-miss transform on the input image
%   using the object to be found as a SE.  If the background of the object
%   is imporant, the BG flag should be TRUE.  If the form of the object is
%   desired anywhere, regardless of background, it should be set to FALSE.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Structural element (Object to find)
%   (3) - Boolean flag, TRUE - BG important, FALSE - BG not important
%
% Output:
%   (1) - Hit-or-Miss of image and object
% 

function [imgOut] = hitormiss(imgIn, obj, bg)

	% Perform first erosion step
	imgOut = morph.erode(imgIn, obj);
	
	% If BG requested, do it
	if bg
		
		% Create SE we need (window larger than object - object)
		[h,w] = size(obj);
		window = true(h,w) & ~obj;
		b2 = true(h+2,w+2);
		b2(2:end-1,2:end-1) = window;
		
		% Erode img complement by b2 SE
		imgComp = ~logical(imgIn);
		imgTemp = morph.erode(imgComp, b2);
		
		% Intersect two outputs for final
		imgOut = imgOut & imgTemp;
		
	end

end
