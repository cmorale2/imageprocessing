%% Morphological Transformation - Closing
% Author: Connor Morales
% 
% Description:
%   This functin performs morphological closing on the input image using
%   the structuring elemnt provided.  Closing is a dilation followed by an
%   erosion.
% 
% Input:
%   (1) - Input binary (logical) image
%   (2) - Structural element (odd dimensions)
%
% Output:
%   (1) - Closing of input by SE
% 

function [imgOut] = close(imgIn, se)

	imgOut = morph.erode(morph.dilate(imgIn,se), se);

end
