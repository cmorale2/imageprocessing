%% Thresholding - Single Value
% Author: Connor Morales
% 
% Description:
%   This function performs basic single thresholding on the input image
%   using the value specified.
% 
%   imgOut = { 1, thresh < imgIn
%            { 0, else
% 
% Input:
%   (1) - Input intensity image
%   (2) - Threshold value
%
% Output:
%   (1) - Output thresholded image (logical)
% 

function [imgOut] = single(imgIn, thresh)

	imgOut = (imgIn > thresh);
	
end
