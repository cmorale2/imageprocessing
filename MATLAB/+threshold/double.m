%% Thresholding - Double Value
% Author: Connor Morales
% 
% Description:
%   This function performs basic double thresholding on the input image
%   using the values specified.
% 
%   imgOut = { 255, thresh(1) < imgIn
%            { 127, thresh(2) < imgIn <= thresh(1)
%            { 0,   else
% 
% Input:
%   (1) - Input intensity image
%   (2) - Threshold values (2-length vector)
%
% Output:
%   (1) - Output thresholded image (logical)
% 

function [imgOut] = double(imgIn, thresh)

	if length(thresh) < 2
		error('Must specify two threshold values');
	end

	imgOut = imgIn;
	imgOut(imgIn > thresh(1)) = 255;
	imgOut(imgIn > thresh(2) & imgIn <= thresh(1)) = 127;
	
end
