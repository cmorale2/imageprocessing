%% Thresholding - Otsu's Method
% Author: Connor Morales
% 
% Description:
%   This function determines a threshold value using Otsu's method.
% 
% Input:
%  (1) - Probability density function (normalized histogram) of image
%
% Output:
%  (1) - Threshold value
% 

function [thresh] = tOtsu(pdf)

	% Number of intensity values (255)
	num = length(pdf);

	% Compute cumulative sums
	cdf = histogram.cumsum(pdf);
	
	% Compute cumulative means
	m = zeros(num, 1);
	for i = 2 : num
		m(i) = m(i-1) + pdf(i)*(i-1);
	end
	
	% Compute global intensity mean
	mg = m(num);
	
	% Compute between-class variance
	s = zeros(num, 1);
	for i = 1 : num
		P1 = cdf(i);
		P2 = 1 - P1;
		
		s(i) = (mg*P1 - m(i))^2 / (P1*P2);
		
	end
	
	% Get values of i where s is max (average if multiple)
	k = 0:255;
	thresh = mean(k(s == max(s)));
	
end
