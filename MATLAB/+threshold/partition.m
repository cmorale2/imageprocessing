%% Thresholding - Partitioning
% Author: Connor Morales
% 
% Description:
%   This function partitions the input image into separate sub-images and
%   performs thresholding on them individually, combining them to a final
%   thresholded image.
%   If the image does not divide evenly into the specified number of
%   partitions, all extra pixels (remaining after division) will be
%   contained in the final partition row/column.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Number of partitions (y,x) (2-length vector)
%
% Output:
%   (1) - Thresholded  image (logical)
%   (2) - Threshold values (matrix, one per partition)
%   (3) - Histograms (cell array, one per partition)
% 

% TODO: add input for different types of thresholding

function [varargout] = partition(imgIn, num)

	% Type forcing
	imgIn = uint8(imgIn);

	% Image dimensions
	[h,w] = size(imgIn);
	
	% Check partitions
	if num(1) > h || num(2) > w
		error('Partition amount must be less than size');
	end
	
	% Initialize output image
	imgOut = false(h,w);
	
	% Initialize temp vars
	imgPart = cell(num(1), num(2));
	thresh = zeros(num(1), num(2));
	hist = cell(num(1), num(2));
	
	sy = floor(h / num(1));
	sx = floor(w / num(2));
	
	for x = 1 : num(2)
		
		% Determine bottom and top x ranges of input images for current
		% partition
		ixb = (x-1)*sx + 1;
		if x == num(2)
			ixt = w;
		else
			ixt = x*sx;
		end
		
		for y = 1 : num(1)
			
			% Determine bottom and top y ranges of input images for current
			% partition
			iyb = (y-1)*sy + 1;
			if y == num(1)
				iyt = h;
			else
				iyt = y*sy;
			end
			
			% Partition image
			imgPart{y,x} = imgIn(iyb:iyt, ixb:ixt);
			% Determine threshold value
			hist{y,x} = histogram.compute(imgPart{y,x});
			thresh(y,x) = threshold.tOtsu(histogram.normalize(hist{y,x}));
			% Threshold image partition and write to output
			imgOut(iyb:iyt, ixb:ixt) = threshold.single(imgPart{y,x}, thresh(y,x));
		end
	end
	
	
	% Set outputs
	if nargout > 3
		error('Too many outputs (%i). Max 3', nargout);
	end
	if nargout > 0
		varargout{1} = imgOut;
	end
	if nargout > 1
		varargout{2} = thresh;
	end
	if nargout > 2
		varargout{3} = hist;
	end
	
end
