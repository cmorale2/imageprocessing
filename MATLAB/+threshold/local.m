%% Thresholding - Local Variable Processing
% Author: Connor Morales
% 
% Description:
%   This function performs variable thresholding based on global mean and
%   local standard deviation.
%   A pixel is set to TRUE if it is greater than the first constant times
%   the local standard deviation and greater than the second constant times
%   the global mean.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Local square size (odd)
%   (3) - Constants (2-length vector) (std, mean)
%
% Output:
%   (1) - Thresholded image (logical)
% 

% TODO: add input for different types of thresholding

function [imgOut] = local(imgIn, fSize, consts)

	% Type forcing
	imgIn = uint8(imgIn);

	% Image dimensions
	[h,w] = size(imgIn);
	
	% Check size
	if mod(fSize,2) ~= 1
		error('Window size must be odd');
	end
	
	% Range for window
	r = -floor(fSize/2) : floor(fSize/2);
	
	% Calculate global mean
	mg = mean(double(imgIn(:)));
	
	% Output image
	imgOut = false(h,w);
	
	% Go through all pixels
	for y = 1 : h
		for x = 1 : w
			
			% X and Y ranges
			rx = x + r;
			ry = y + r;
			
			% Limit to within range
			rx = rx(rx > 1 & rx <= w);
			ry = ry(ry > 1 & ry <= h);
			
			% Find mean and standard deviation
			temp = imgIn(ry,rx);
			s = std(double(temp(:)));
			
			% Set output
			imgOut(y,x) = (imgIn(y,x) > consts(1)*s & imgIn(y,x) > consts(2)*mg);
			
		end
	end
	
end
