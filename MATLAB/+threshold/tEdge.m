%% Thresholding - Edge Detection
% Author: Connor Morales
% 
% Description:
%   This function determines a threshold value using edge detection.  Edges
%   of the image are detected, and only edge pixels above a certain
%   percentile are used in Otsu's threshold algorithm.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Type of edge detection (magnitude, laplacian)
%   (3) - Percentile for edge pixels
%
% Output:
%   (1) - Threshold value
% 

function [thresh] = tEdge(imgIn, type, pcent)

	% Calculate edge image
	if strcmpi(type, 'magnitude')
		% Calculate  gradients
		Gx = spatial.convolve(imgIn, spatial.kernelSobel(true));
		Gy = spatial.convolve(imgIn, spatial.kernelSobel(false));

		% Calculate magnitude
		imgEdge = abs(Gx) + abs(Gy);
		
	elseif strcmpi(type, 'laplacian')
		% Calculate laplacian
		imgEdge = spatial.convolve(imgIn, spatial.kernelLaplace(true));
		
	else
		error('Unknown type: %s', type);
	end
	
	% Threshold on edge image
	imgThresh = (imgEdge > prctile(imgEdge(:),pcent));
	
	% Get PDF
	pdf = histogram.normalize(histogram.compute(imgIn(imgThresh)));
	
	% Perform Otsu's method on edge pixels
	thresh = threshold.tOtsu(pdf);
	
end
