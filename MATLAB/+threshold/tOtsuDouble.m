%% Thresholding - Two-Valued Otsu's Method
% Author: Connor Morales
% 
% Description:
%   This function extracts two threshold values from the input image using
%   a generalized Otsu's method.
% 
% Input:
%  (1) - Probability density function (normalized histogram) of image
%
% Output:
%  (1) - Threshold values (high, low)
% 

function [thresh] = tOtsuDouble(pdf)

	% Number of intensity values (255)
	num = length(pdf);

	% Compute cumulative sums
	cdf = histogram.cumsum(pdf);
	
	% Compute cumulative means
	m = zeros(num, 1);
	for i = 2 : num
		m(i) = m(i-1) + pdf(i)*(i-1);
	end
	
	% Compute global intensity mean
	mg = m(num);
	
	% Initialize between-class variance (k1,k2)
	s = zeros(num,num);
	
	% Iterate through all k1,k2 pairs
	for i1 = 2 : num-2
		% Probability sum for 1:k1
		P1 = cdf(i1);
		% Mean sum for 1:k1
		m1 = m(i1) / P1;
		
		% Iterate through all k2 pairs given k1
		for i2 = i1+1 : num-1
			% Probability sums for k1+1:k2 and k2+1:num
			P2 = cdf(i2) - cdf(i1);
			P3 = cdf(end)- cdf(i2);
			% Mean sums for same ranges
			m2 = (m(i2) - m(i1)) / P2;
			m3 = (m(end)- m(i2)) / P3;
			
			% Calculate between-class variance
			s(i1,i2) = P1*(m1-mg)^2 + P2*(m2-mg)^2 + P3*(m3-mg)^2;
		end
	end
	
	% Find max values for k1,k2
	[k1,k2] = find(s == max(s(:)));
	
	% Shift to range 0:255
	k1 = k1 - 1;
	k2 = k2 - 1;
	
	% Mean of these values is output
	thresh = [mean(k2), mean(k1)];
		
end
