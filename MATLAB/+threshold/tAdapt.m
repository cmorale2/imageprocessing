%% Thresholding - Basic Adaptive
% Author: Connor Morales
% 
% Description:
%   This function determines a threshold value based on the mean
%   intensity values in the input image.  The algorithm is iterative and
%   will continue until the change in final threshold value is under a
%   specific value.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Optional - Delta threshold value (default 0.01)
%
% Output:
%   (1) - Threshold value
% 

function [Tcur] = tAdapt(imgIn, varargin)

	% Delta T value
	if nargin > 1
		dt = varargin{1};
	else
		dt = 0.01;
	end

	% Flatten image
	imgTemp = imgIn(:);

	% Initial estimate
	Tcur = mean(imgTemp);
	Told = Inf(1);
	
	% Adaptive loop
	while abs(Tcur-Told) >= dt
		
		% Segment image with current threshold
		imgThresh = (imgTemp > Tcur);
		
		% Calculate mean of both sides
		mhi  = mean(imgTemp(imgThresh));
		mlow = mean(imgTemp(~imgThresh));
		
		% Calculate new thresh
		Told = Tcur;
		Tcur = (mhi + mlow) / 2;
		
	end
	
end
