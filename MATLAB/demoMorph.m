%% Morphology Package Demo
% Author: Connor Morales

% TODO: add multiple hole fill

%% Cleanup
close all
clear
clc
pause(0.1)


%% Erosion

img = imread('../Images/wirebond.tif');

imgNew = cell(3,1);

fprintf('Eroding...');
t = tic();
imgNew{1} = morph.erode(img, true(11));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Eroding...');
t = tic();
imgNew{2} = morph.erode(img, true(15));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Eroding...');
t = tic();
imgNew{3} = morph.erode(img, true(45));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Erosion', 'ToolBar','none');
subplot(2,2,1)
imshow(img)
title('Original');
subplot(2,2,2)
imshow(imgNew{1})
title('Size 11')
subplot(2,2,3)
imshow(imgNew{2})
title('Size 15')
subplot(2,2,4)
imshow(imgNew{3})
title('Size 45')

pause(0.1)


%% Dilation

img = imread('../Images/text.tif');

fprintf('Dilating...');
t = tic();
imgNew = morph.dilate(img, morph.seSphere(1));
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Dilation', 'ToolBar','none');
subplot(1,2,1)
imshow(img);
title('Original');
subplot(1,2,2)
imshow(imgNew)
title('R1 Sphere');


%% Opening and Closing

img = imread('../Images/fingerprint_b.tif');

se = true(3);

fprintf('Opening...');
t = tic();
imgOpen  = morph.open(img, se);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

fprintf('Closing...');
t = tic();
imgClose = morph.close(imgOpen, se);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);


figure('Name','Opening and Closing', 'ToolBar','none');
subplot(1,3,1)
imshow(img);
title('Original');
subplot(1,3,2)
imshow(imgOpen)
title('Opened');
subplot(1,3,3)
imshow(imgClose)
title('Closed')


%% Boundary Extraction

img = imread('../Images/lincoln.tif');

fprintf('Boundary Extraction...');
t = tic();
imgBound = morph.boundary(img);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Boundary Extraction', 'ToolBar','none');
subplot(1,2,1)
imshow(img);
title('Original')
subplot(1,2,2)
imshow(imgBound)
title('Boundary')


%% Hole Filling

img = logical(imread('../Images/hole.tif'));

fprintf('Hole Filling...');
t = tic();
imgHole = morph.holefill(img, [150,150]);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Single Hole Fill', 'ToolBar','none');
subplot(1,3,1)
imshow(img)
title('Original')
subplot(1,3,2)
imshow(imgHole)
title('Hole Filled')
subplot(1,3,3)
imshow(img | imgHole)
title('Combined')


%% Thinning

img = logical(imread('../Images/utk.tif'));

fprintf('Thinning...');
t = tic();
imgThin = morph.thin(img);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Thinning', 'ToolBar','none')
subplot(1,2,1);
imshow(img)
title('Original');
subplot(1,2,2)
imshow(imgThin)
title('Thinned');


%% Skeleton

img = logical(imread('../Images/utk.tif'));

fprintf('Skeleton...');
t = tic();
imgSkel = morph.skeleton(img, 100);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Skeleton', 'ToolBar','none')
subplot(1,2,1)
imshow(img)
title('Original')
subplot(1,2,2)
imshow(imgSkel)
title('Skeleton')


%% Hit-or-Miss

img = logical(imread('../Images/utk.tif'));

imgObj = img(113:143, 110:144);

fprintf('Hit or Miss...');
t = tic();
imgHit = morph.hitormiss(img, imgObj, true);
tF = toc(t);
fprintf('Done! (%.2f s)\n', tF);

figure('Name','Hit or Miss', 'ToolBar','none')
subplot(1,2,1)
imshow(img)
title('Original');
subplot(1,2,2)
imshow(imgHit)
title('Hit-or-Miss')

