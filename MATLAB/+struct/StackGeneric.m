%% Stack Class
% Author: Connor Morales
% 
% Generic stack class that can accept any data type needed.  Everything
% that is pushed is given its own cell in an array.
% 
% Properties:
%   Data  -> Cell array of data stored
%   Index -> Index of current entry in cell array (starts at 1)
% 
% Methods:
%   isEmpty -> boolean of whether stack is empty or not
%   contains-> boolean of whether stack contains provided data
%   Push    -> pushes data onto stack, no return value
%   Pop     -> returns top data off stack, error if stack is empty
% 

classdef StackGeneric < handle
	
	properties(GetAccess = public, SetAccess = private)
		% Cell array of data
		Data
		% Index of current cell
		Index
	end
	
	methods(Access = public)
		
		function obj = StackGeneric()
			obj.Data = {};
			obj.Index = uint64(0);
		end
	
		% Check if stack is empty
		function b = isEmpty(obj)
			b = (obj.Index == 0);
		end
		
		% Check if data is in stack
		function tf = contains(obj, data)
			tf = false;
			for i = 1 : obj.Index
				tf = isequal(obj.Data{i}, data);
				if tf
					return;
				end
			end
		end
	
		% Push data onto stack
		function Push(obj, toPush)
			% increment index first
			obj.Index = obj.Index + 1;
			% add new data
			obj.Data{obj.Index} = toPush;
		end
		
		% Pop data off of stack
		function popped = Pop(obj)
			if obj.Index > 0
				% output data
				popped = obj.Data{obj.Index};
				% clear cell
				obj.Data{obj.Index} = [];
				% decrement index
				obj.Index = obj.Index - 1;
			else
				error('Error: attempted to pop an empty stack');
			end
		end
		
	end
	
end
