%% Static Priority Queue Class
% Author: Connor Morales
% 
% Generic queue class that can accept any data type needed.  Everything
% that is queued is given its own cell in an array.
% 
% Properties:
%   Data      -> Cell array of generic queues for each priority
%   NumPriors -> Number of priorities queue can handle
% 
% Methods:
%   isEmpty -> Checks if queue is empty, logical
%   Enqueue -> Adds data with given priority to queue
%   Dequeue -> Removes datum with least priority (empty matrix if none)
% 

classdef QueuePriorityStatic < handle
	
	properties(GetAccess = public, SetAccess = private)
		% Cell array of data
		Data
		% Number of priorities
		NumPriors
	end
	
	methods(Access = public)
		
		function obj = QueuePriorityStatic(num)
			obj.Data = cell(num,1);
			obj.NumPriors = uint64(num);
			for i = 1 : num
				obj.Data{i} = struct.QueueGeneric();
			end
		end
		
		% Check if empty
		function tf = isEmpty(obj)
			tf = true;
			for i = 1 : obj.NumPriors
				if ~obj.Data{i}.isEmpty()
					tf = false;
					break;
				end
			end
		end
		
		% Add element to queue
		function Enqueue(obj, data, prior)
			% Sanity check
			if prior > obj.NumPriors
				error('Given priority (%i) larger than max (%i)', proir, obj.NumPriors);
			end
			% Add data
			obj.Data{prior}.Enqueue(data);
		end
		
		% Remove element from queue
		function data = Dequeue(obj)
			data = [];
			for i = 1 : obj.NumPriors
				if ~obj.Data{i}.isEmpty()
					data = obj.Data{i}.Dequeue();
					break;
				end
			end
		end
		
	end
	
end


