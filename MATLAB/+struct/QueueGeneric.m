%% Generic Queue Class
% Author: Connor Morales
% 
% Generic queue class that can accept any data type needed.  Everything
% that is queued is given its own cell in an array.
% 
% Properties:
%   Data  -> Cell array of data stored
%   IndexFront -> Index of front of queue
%	IndexBack  -> Index of back of queue
% 
% Methods:
%   Reset   -> resets queue and empties
%   isEmpty -> boolean of whether queue is empty or not
%   Count   -> number of elements in queue
%   Enqueue -> adds data to back of queue
%   Dequeue -> reads data from front of queue (empty matrix if none)
% 

classdef QueueGeneric < handle
	
	properties(GetAccess = public, SetAccess = private)
		% Cell array of data
		Data
		% Index of current cell
		IndexFront
		IndexBack
	end
	
	methods(Access = public)
		
		function obj = QueueGeneric()
			obj.Data = {};
			obj.IndexFront = uint64(1);
			obj.IndexBack  = uint64(1);
		end
		
		% Reset queue to empty
		function Reset(obj)
			obj.Data = {};
			obj.IndexFront = uint64(1);
			obj.IndexBack = uint64(1);
		end
	
		% Check if queue is empty
		function b = isEmpty(obj)
			b = (obj.IndexFront == obj.IndexBack);
		end
		
		% Determine number of elements in queue
		function n = Count(obj)
			n = (obj.IndexBack - obj.IndexFront);
		end
	
		% Add data to back of queue
		function Enqueue(obj, toAdd)
			% Add new data
			obj.Data{obj.IndexBack} = toAdd;
			% Shift index for back of queue
			obj.IndexBack = obj.IndexBack + 1;
		end
		
		% Read data from front of queue
		function toRet = Dequeue(obj)
			if obj.IndexFront ~= obj.IndexBack
				% Save output data
				toRet = obj.Data{obj.IndexFront};
				% Clear queue data
				obj.Data{obj.IndexFront} = [];
				% Shift index for front of queue
				obj.IndexFront = obj.IndexFront + 1;
				% Reset queue if empty
				if obj.IndexFront == obj.IndexBack
					obj.Reset();
				end
			else
				toRet = [];
			end
		end
		
	end
	
end


