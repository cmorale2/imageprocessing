%% Segmentation - Region Dividing
% Author: Connor Morales
% 
% Description:
%   This function is similar to the Region Splitting (rsplit) function
%   except it divides the image into regions of size (minsize x minsize).
%   Each of these regions is analyzed using a predicate Q.
%
%   Q = { TRUE,  C1 < std < C2  &&  C3 < mean < C4
%       { FALSE, else
% 
%   where {C} is the set of constants provided to the function.  If Q
%   evaluates as TRUE, the entire region is set to TRUE in the output
%   image, and vice versa.
%
% NOTE:
%   If the image does not divide evenly by minsize, all extra pixels
%   (remainders from division) will be added to the last region division in
%   each direction.  This means that if both X and Y dimensions do not
%   divide evenly, the bottom rightmost division will be the largest.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Region size (px)
%   (3) - Constants (4-length vector)
%
% Output:
%   (1) - Segmented image (logical)
% 

function [imgOut] = rdivide(imgIn, minsize, consts)

	% Type forcing
	imgIn = double(imgIn);

	% Sizing
	[h,w] = size(imgIn);
	nx = floor(w / minsize);
	ny = floor(h / minsize);
	
	% Initialize output
	imgOut = false(h,w);
	
	% Go through all subdivisions
	for x = 1 : nx
		
		% Find low and high ranges for X
		ixs = (x-1)*minsize + 1;
		if x == nx
			ixe = w;
		else
			ixe = ixs + minsize;
		end
		
		for y = 1 : ny
			
			% Find low and high ranges for Y
			iys = (y-1)*minsize + 1;
			if y == ny
				iye = h;
			else
				iye = iys + minsize;
			end
			
			% Calculate mean and std of subdivision
			imgSub = imgIn(iys:iye, ixs:ixe);
			m = mean(imgSub(:));
			s = std(imgSub(:));
			
			% Set output based on constants
			imgOut(iys:iye, ixs:ixe) = (consts(1) < s & s < consts(2) & consts(3) < m & m < consts(4));
			
		end
	end
	
	% Type forcing
	imgOut = logical(imgOut);
	
end
