%% Segmentation - Region Splitting
% Author: Connor Morales
% 
% Description:
%   This function performs region splitting to segment the input image.
%   For each region, starting with the entire image, the mean and standard
%   deviation are calculated and evaluated with a predicate Q.
%   
%   Q = { TRUE,  C1 < std < C2  &&  C3 < mean < C4
%       { FALSE, else
% 
%   where {C} is the set of constants provided to the function.  If a
%   region's predicate evaluates as FALSE, it is divided into 4
%   subdivisions, and the entire process is repeated for each subdivision.
%   If the predicate evaluates as TRUE, the entire region is set to TRUE in
%   the output image.
%   If dividing a region would result in subdivisions with a dimension
%   smaller than the minimum size specified, it is not divided any further.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Minimum region size (px)
%   (3) - Constants (4-length vector)
%
% Output:
%   (1) - Segmented image (logical)
% 

function [imgOut] = rsplit(imgIn, minsize, consts)

	% Type forcing
	imgIn = double(imgIn);

	% Sizing
	[h,w] = size(imgIn);
	hx = floor(w/2);
	hy = floor(h/2);

	% Structure
	root = struct;
	root.type = 'tree';
	root.xs = 1;
	root.xe = w;
	root.ys = 1;
	root.ye = h;
	
	% Initialize output
	imgOut = false(h,w);
	
	% Recurse through tree as needed
	[~, imgOut] = split_node(root, imgIn, imgOut, minsize, consts);
	
	% Type forcing
	imgOut = logical(imgOut);
	
end



% imgOut initialized to FALSE
function [node, imgOut] = split_node(node, imgIn, imgOut, minsize, consts)

	% Get subimage
	imgSub = imgIn(node.ys:node.ye, node.xs:node.xe);

	% Calculate mean/std
	node.m = mean(imgSub(:));
	node.s = std(imgSub(:));

	% Check if current node is finished and set output TRUE if so
	if node.s > consts(1) && node.s < consts(2) && node.m > consts(3) && node.m < consts(4)
		node.type = 'leaf';
		imgOut(node.ys:node.ye, node.xs:node.xe) = true;
		return;
	end
	
	% Check if we can't subdivide anymore
	hx = floor( (node.xe-node.xs) / 2 );
	hy = floor( (node.ye-node.ys) / 2 );
	if hx < minsize || hy < minsize
		node.type = 'leaf';
		return;
	end
	
	% We need to subdivide current node
	node.type = 'tree';
	node.quads = cell(2,2);
	
	
	% Top left region
	node.quads{1,1} = struct;
	node.quads{1,1}.xs = node.xs;
	node.quads{1,1}.xe = node.xs + hx;
	node.quads{1,1}.ys = node.ys;
	node.quads{1,1}.ye = node.ys + hy;
	[node.quads{1,1}, imgOut] = split_node(node.quads{1,1}, imgIn, imgOut, minsize, consts);
	
	% Bottom left region
	node.quads{2,1} = struct;
	node.quads{2,1}.xs = node.xs;
	node.quads{2,1}.xe = node.xs + hx;
	node.quads{2,1}.ys = node.ys + hy + 1;
	node.quads{2,1}.ye = node.ye;
	[node.quads{2,1}, imgOut] = split_node(node.quads{2,1}, imgIn, imgOut, minsize, consts);
	
	% Top right region
	node.quads{1,2} = struct;
	node.quads{1,2}.xs = node.xs + hx + 1;
	node.quads{1,2}.xe = node.xe;
	node.quads{1,2}.ys = node.ys;
	node.quads{1,2}.ye = node.ys + hy;
	[node.quads{1,2}, imgOut] = split_node(node.quads{1,2}, imgIn, imgOut, minsize, consts);
	
	% Bottom right region
	node.quads{2,2} = struct;
	node.quads{2,2}.xs = node.xs + hx + 1;
	node.quads{2,2}.xe = node.xe;
	node.quads{2,2}.ys = node.ys + hy + 1;
	node.quads{2,2}.ye = node.ye;
	[node.quads{2,2}, imgOut] = split_node(node.quads{2,2}, imgIn, imgOut, minsize, consts);

end
