%% Segmentation - Watershed Brute Force Implementation
% Author: Connor Morales
% 
% Description:
%   This function performs watershed segmentation on the input image.
%   This implementation of watershed is a brute-force adaptation of the
%   algorithm found in Digital Image Processing 3rd Edition by Gonzalez and
%   Woods.
% 
% Input:
%   (1) - Input intensity image
%
% Output:
%   (1) - Edge image (logical)
%   (2) - Labeled image (uint32) (edges are labeled 0)
% 

function [varargout] = watershedBrute(imgIn)

	% Big value for dams
	BIGVALUE = uint32(Inf);

	% Sizing
	[h,w] = size(imgIn);
	
	% Initialize output
	imgOut = uint32(zeros(h,w));
	
	% Type forcing
	imgIn = uint8(imgIn);
	
	% Get list of all intensity values in image
	vals = sort(unique(imgIn(:)), 'ascend');
	
	% Initialize label
	lCur = 1;
	
	% Initialize watersheds with lowest intensity value
	[y,x] = find(imgIn == vals(1));
	T = [y,x];
	for i = 1 : size(T,1)
		if imgOut(T(i,1), T(i,2)) == 0
			imgOut = internal.floodfill4(imgIn, imgOut, T(i,:), lCur);
			lCur = lCur + 1;
		end
	end
	
	% Initialize watershed lists
	C = cell(lCur-1, 1);
	for i = 1 : lCur-1
		[y,x] = find(imgOut == i);
		C{i} = [y,x];
	end
	
	% We will need to keep track of previous component sizes of T
	prevQ = [];
	
	% Go through all intensity values
	for v = vals(2:end)'
		
		% Get list of all pixels with this intensity or lower
		[y,x] = find(imgIn <= v);
		T = [y,x];
		imgT = (imgIn <= v);
		
		% Find all connected components in T
		imgTemp = zeros(h,w);
		lLoc = 1;
		for i = 1 : size(T,1)
			if imgTemp(T(i,1), T(i,2)) == 0
				imgTemp = internal.floodfill4(imgT, imgTemp, T(i,:), lLoc);
				lLoc = lLoc + 1;
			end
		end
		
		% Initialize component lists for current intensity level
		Q = cell(lLoc-1, 1);
		prevQ = [prevQ; zeros(lLoc-1 - length(prevQ), 1)];
		
		% Create set of components that exist at current intensity level
		for q = 1 : lLoc-1
			% Get list of pixels in current component
			[y,x] = find(imgTemp == q);
			Q{q} = [y,x];
			
			% Go through all watershed components and see if any intersect
			% with the current component.
			CaT = cell(length(C), 1);
			num = zeros(length(C), 1);
			for c = 1 : length(C)
				CaT{c} = intersect(C{c}, Q{q}, 'rows');
				num(c) = size(CaT{c}, 1);
			end
			
			% Check how many overlaps we have
			
			% If this component does not overlap with a watershed, create a
			% new watershed
			if sum(num > 0) == 0
				% No overlap, so q is a new watershed component
				% Add pixels in q as a new watershed
				C = [C; Q{q}];
				% Update output image
				for i = 1 : size(Q{q}, 1)
					imgOut(Q{q}(i,1), Q{q}(i,2)) = lCur;
				end
				lCur = lCur + 1;
				
			% If this component overlaps with one watershed AND the
			% components are not the same, update the watershed with new
			% pixels
			elseif sum(num > 0) == 1 && size(C{find(num>0)},1) ~= size(Q{q},1)
				% Overlap with a single watershed, so q just replaces that
				% watershed component
				i = find(num>0);
				lTemp = imgOut(C{i}(1,1), C{i}(1,2));
				C{i} = Q{q};
				% Update output image
				for i = 1 : size(Q{q}, 1)
					imgOut(Q{q}(i,1), Q{q}(i,2)) = lTemp;
				end
				
			% If this component overlaps with two or more watersheds AND we
			% have more pixels in the component than we have previously, we
			% need to expand the watersheds and build dams
			elseif sum(num > 0) > 1 && length(y) > prevQ(q)
				% We need to build dams
				
				% Update size of component
				prevQ(q) = length(y);
				
				% Special dilation loop
				while true
					
					% Go through all pixels in q
					cnt = 0;
					for i = 1 : size(Q{q}, 1)

						% Current pixel
						pix = Q{q}(i,:);
						y = pix(1);
						x = pix(2);

						% Check if this pixel is dam or labeled
						if imgOut(y,x) > 0
							cnt = cnt + 1;
							continue;
						end

						% Special dilation:
						% Make list of all N8 neighbor labels
						plist = zeros(8,1);
						% Right
						if x<w
							plist(1) = imgOut(y,x+1);
						end
						% Top Right
						if x<w && y>1
							plist(2) = imgOut(y-1,x+1);
						end
						% Top
						if y>1
							plist(3) = imgOut(y-1,x);
						end
						% Top Left
						if x>1 && y>1
							plist(4) = imgOut(y-1,x-1);
						end
						% Left
						if x>1
							plist(5) = imgOut(y,x-1);
						end
						% Bottom Left
						if x>1 && y<h
							plist(6) = imgOut(y+1,x-1);
						end
						% Bottom
						if y<h
							plist(7) = imgOut(y+1,x);
						end
						% Bottom Right
						if x<w && y<h
							plist(8) = imgOut(y+1,x+1);
						end

						% Extract unique labels from list
						plist = unique(plist(plist > 0 & plist < BIGVALUE));

						% If only one unique label, set center pixel to that
						% label and add pixel to watershed list
						if length(plist) == 1
							imgOut(y,x) = plist(1);
							C{plist(1)} = [C{plist(1)}; [y,x]];

						% If more than one unique label, center pixel should
						% be set to a dam
						else
							imgOut(y,x) = BIGVALUE;
						end

					end

					% End condition will be when all pixels in q have a label
					% (are nonzero)
					if cnt == size(Q{q},1)
						break;
					end
					
				end
				
				
			end
			
		end
		
		
	end
	
	
	% First output is a logical image of dam lines (borders between
	% watersheds
	if nargout > 0
		varargout{1} = (imgOut == BIGVALUE);
	end
	% Second output is image of labels (uint32).  All dams should have
	% value 0
	if nargout > 1
		varargout{2} = uint32(imgOut);
		varargout{2}(imgOut == BIGVALUE) = uint32(0);
	end
	
	
	
end
