%% Segmentation - Region Growing
% Author: Connor Morales
% 
% Description:
%   This function segments the input intensity image using region growing.
%   Regions are started at seed locations and expanded in N8 following an
%   absolute value predicate:
% 
%   Q = { TRUE, abs( intensity(seed) - intensity(current pixel) ) <= T
%       { FALSE, else
%   
%   In words, the predicate is TRUE if the absolute difference in intensity
%   between the current pixel and its respective seed pixel is below or
%   equal to the provided threshold.
% 
% NOTE:
%   If a region growth from a seed pixel completely envelops a seed pixel
%   farther down the seed list (overlaps will all N8 neighbors), the later
%   seed pixel will be ignored.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Region seeds (nx2 matrix) (y,x)
%   (3) - Threshold value for predicate
%
% Output:
%   (1) - Binary image, all regions TRUE
% 

function [imgOut] = rgrow(imgIn, seeds, thresh)

	% Type forcing (for subtraction)
	imgIn = int16(imgIn);

	% Sizing
	[h,w] = size(imgIn);
	ns = size(seeds,1);
	
	% Initialize output image
	imgOut = false(h,w);
	
	% Initialize frontier
	frontier = struct.StackGeneric();
	
	% Set seed points to TRUE in output
	for is = 1 : ns
		imgOut(seeds(is,1), seeds(is,2)) = true;
	end
	
	% Go through all seed points and grow components
	for is = 1 : ns
		
		% Current seed pixel as reference
		pCur = seeds(is,:);
		yc = pCur(1);
		xc = pCur(2);
		
		% Place current seed into frontier
		frontier.Push(pCur);
		
		% Go through entire frontier
		while ~frontier.isEmpty()
			
			% Get current pixel
			pix = frontier.Pop();
			y = pix(1);
			x = pix(2);
			
			% Go through all N8 pixels, setting them to TRUE in output and
			% adding them to frontier IF they are in image bounds, not
			% already TRUE in output, and absolute difference between them
			% and their respective seed pixel does not exceed threshold
			
			% Right
			if x < w && ~imgOut(y,x+1) && abs(imgIn(yc,xc)-imgIn(y,x+1)) <= thresh
				imgOut(y,x+1) = true;
				frontier.Push([y,x+1]);
			end
			
			% Top Right
			if x < w && y > 1 && ~imgOut(y-1,x+1) && abs(imgIn(yc,xc)-imgIn(y-1,x+1)) <= thresh
				imgOut(y-1,x+1) = true;
				frontier.Push([y-1,x+1]);
			end
			
			% Top
			if y > 1 && ~imgOut(y-1,x) && abs(imgIn(yc,xc)-imgIn(y-1,x)) <= thresh
				imgOut(y-1,x) = true;
				frontier.Push([y-1,x]);
			end
			
			% Top Left
			if x > 1 && y > 1 && ~imgOut(y-1,x-1) && abs(imgIn(yc,xc)-imgIn(y-1,x-1)) <= thresh
				imgOut(y-1,x-1) = true;
				frontier.Push([y-1,x-1]);
			end
			
			% Left
			if x > 1 && ~imgOut(y,x-1) && abs(imgIn(yc,xc)-imgIn(y,x-1)) <= thresh
				imgOut(y,x-1) = true;
				frontier.Push([y,x-1]);
			end
			
			% Bottom Left
			if x > 1 && y < h && ~imgOut(y+1,x-1) && abs(imgIn(yc,xc)-imgIn(y+1,x-1)) <= thresh
				imgOut(y+1,x-1) = true;
				frontier.Push([y+1,x-1]);
			end
			
			% Bottom
			if y < h && ~imgOut(y+1,x) && abs(imgIn(yc,xc)-imgIn(y+1,x)) <= thresh
				imgOut(y+1,x) = true;
				frontier.Push([y+1,x]);
			end
			
			% Bottom Right
			if x < w && y < h && ~imgOut(y+1,x+1) && abs(imgIn(yc,xc)-imgIn(y+1,x+1)) <= thresh
				imgOut(y+1,x+1) = true;
				frontier.Push([y+1,x+1]);
			end
			
		end
		
	end

	% Type forcing
	imgOut = logical(imgOut);
	
end
