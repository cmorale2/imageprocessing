%% Segmentation - Marker Watershed Priority Flood Implementation
% Author: Connor Morales
% 
% Description:
%   This function performs watershed segmentation with markers using the
%   priority flooding method.
% 
% Input:
%   (1) - Input intensity image
%   (2) - Marker locations (y,x) (nx2)
%
% Output:
%   (1) - Labeled image (uint64)
% 

function [imgLabel] = watershedmarkPFlood(imgIn, seed)

	% Type forcing and set to range 1:256 (to align with priorities)
	imgIn = uint64(uint8(imgIn));
	imgIn = imgIn - min(imgIn(:)) + 1;
	
	% Sizings
	[h,w] = size(imgIn);
	
	% Initialize output image
	imgLabel = uint64(zeros(h,w));
	
	% Initialize priority queue
	queue = struct.QueuePriorityStatic(256);
	
	
	% Go through marker image and floodfill with initial labels
	% Also find border and add pixels to queue
	curLabel = 1;
	for i = 1 : size(seed,1)
		imgLabel = internal.floodfill4(imgIn, imgLabel, [seed(i,1), seed(i,2)], curLabel);
		curLabel = curLabel + 1;
		q = internal.wallfollow(imgLabel, [seed(i,1), seed(i,2)]);
		while ~q.isEmpty()
			p = q.Dequeue();
			queue.Enqueue([p(1), p(2)], imgIn(p(1),p(2)));
		end
	end
	
	
	
	while true
		
		% Pop pixel from queue
		pix = queue.Dequeue();
		
		% Quit if no more pixels
		if isempty(pix)
			break;
		end
		
		% Typesaving
		y = pix(1);
		x = pix(2);
		
		% Go through pixel neighbors, starting with N4 and ending with
		% diagonals
		% If they already have a label, ignore them
		% Else:
		%   Set their label to same as current pixel
		%   Add to queue
		
		% Right
		if x<w && imgLabel(y,x+1)==0
			imgLabel(y,x+1) = imgLabel(y,x);
			queue.Enqueue([y,x+1], imgIn(y,x+1));
		end
		
		% Top
		if y>1 && imgLabel(y-1,x)==0
			imgLabel(y-1,x) = imgLabel(y,x);
			queue.Enqueue([y-1,x], imgIn(y-1,x));
		end
		
		% Left
		if x>1 && imgLabel(y,x-1)==0
			imgLabel(y,x-1) = imgLabel(y,x);
			queue.Enqueue([y,x-1], imgIn(y,x-1));
		end
		
		% Bottom
		if y<h && imgLabel(y+1,x)==0
			imgLabel(y+1,x) = imgLabel(y,x);
			queue.Enqueue([y+1,x], imgIn(y+1,x));
		end
		
		
		% Top Right
		if x<w && y>1 && imgLabel(y-1,x+1)==0
			imgLabel(y-1,x+1) = imgLabel(y,x);
			queue.Enqueue([y-1,x+1], imgIn(y-1,x+1));
		end
		
		% Top Left
		if x>1 && y>1 && imgLabel(y-1,x-1)==0
			imgLabel(y-1,x-1) = imgLabel(y,x);
			queue.Enqueue([y-1,x-1], imgIn(y-1,x-1));
		end
		
		% Bottom Left
		if x>1 && y<h && imgLabel(y+1,x-1)==0
			imgLabel(y+1,x-1) = imgLabel(y,x);
			queue.Enqueue([y+1,x-1], imgIn(y+1,x-1));
		end
		
		% Bottom Right
		if x<w && y<h && imgLabel(y+1,x+1)==0
			imgLabel(y+1,x+1) = imgLabel(y,x);
			queue.Enqueue([y+1,x+1], imgIn(y+1,x+1));
		end
		
	end
	
end
